<?php

Route::get('/', array('as'=>'principal.home', 'uses' =>'PrincipalController@home'));
Route::get('/gracias', array('as'=>'principal.gracias', 'uses' =>'PrincipalController@gracias'));
Route::get('/seguro/{seguro}', array('as'=>'principal.seguro', 'uses' =>'PrincipalController@seguro'));
Route::get('/marca/{marca}', array('as'=>'principal.marca', 'uses' =>'PrincipalController@marca'));

Route::get('/resultado', function(){
  		
	return View::make('resultado.main', compact('marca','uso','anio'));
});

Route::get('/nuevo', function(){

	$marca = DB::table('marca')->select('id_marca','name_marca')->get();
	$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
	$anio = DB::table('anio')->select('id_anio','anio_antig')->get();
  		
	return View::make('old.principal.home', compact('marca','uso','anio'));
});

Route::get('emails/default', function(){

	return View::make('emails.default');

});


// Route::get('/modelos/lista', function(){

// 	$marca = DB::table('marca')->select('id_marca','name_marca')->get();
// 	$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
// 	$anio = DB::table('anio')->select('id_anio','anio_antig')->get();
  		
// 	return View::make('old.principal.home', compact('marca','uso','anio'));
// });

Route::post('/modelos/lista', array('as' => 'modelos.lista', function()
{
    $marca = Input::get('id_marca');

	$modelos = DB::table('auto_valor')
			  	->join('modelo','auto_valor.id_modelo','=','modelo.id_model')
			  	->where('auto_valor.id_marca','=',$marca)
			  	->select('modelo.id_model','modelo.name_model')
			  	->get();
		echo '<option value="?">Elige modelo</option>';
	foreach ($modelos as $key => $modelo) {
		echo '<option value="'.$modelo->id_model.'">'.$modelo->name_model.'</option>';
	}

}));

Route::post('/tipo/lista', array('as' => 'tipo.lista', function()
{
    $modelo = Input::get('id_modelo');

	$tipos = DB::table('auto_valor')
			  	->join('clase','auto_valor.id_tipo','=','clase.id_clase')
			  	->where('auto_valor.id_modelo','=',$modelo)
			  	->select('clase.id_clase','clase.clase')
			  	->get();
	echo '<option value="?">Elige tipo</option>';
	foreach ($tipos as $key => $tipo) {
		echo '<option value="'.$tipo->id_clase.'">'.$tipo->clase.'</option>';
	}

}));

Route::post('/uso/lista', array('as' => 'uso.lista', function()
{
    $id_tipo = Input::get('id_tipo');

	$usos = DB::table('tipouso')
			  	->join('uso','tipouso.uso','=','uso.id_uso')
			  	->where('tipouso.tipo','=',$id_tipo)
			  	->select('uso.id_uso','uso.uso')
			  	->get();
	echo '<option value="?">Elige Uso</option>';
	foreach ($usos as $key => $uso) {
		echo '<option value="'.$uso->id_uso.'">'.$uso->uso.'</option>';
	}

}));


Route::post('/form/store', array('as' => 'form.store', function()
{
	$objeto = new Formulario();
	$objeto->marca_id				= Input::get('marca');
	$objeto->modelo_id				= Input::get('modelo');
	$objeto->tipo_id				= Input::get('tipo');
	$objeto->anio_id				= Input::get('anio');
	$objeto->nuevo_usado			= Input::get('nuevo_usado');
	$objeto->uso_id					= Input::get('uso');
	$objeto->valor_actual			= Input::get('valor_actual');
	$objeto->ubicacion				= Input::get('ubicacion');
	$objeto->nombre					= Input::get('nombre');
	$objeto->email					= Input::get('email');
	$objeto->celular				= Input::get('celular');
	$objeto->save();


	$emisorEmail 	= 'aseguratuauto@hermes.pe';
	$receptorEmail 	= 'aseguratuauto@hermes.pe';
	$emisorName 	= 'Asegura tu Auto - Cotización Web';
	$subjectFixed   = 'Asegura tu Auto - Cotización Web';
	$mensaje 		= 'Defecto';

	$marca   		=		Input::get('marca');
	$modelo    		= 		Input::get('modelo');
	$tipo   		=		Input::get('tipo');
	$anio   		=		Input::get('anio');
	$nuevo_usado   	=		Input::get('nuevo_usado');
	$uso   			=		Input::get('uso');
	$valor_actual   =		Input::get('valor_actual');
	$ubicacion   	=		Input::get('ubicacion');
	$nombre   		=		Input::get('nombre');
	$email   		=		Input::get('email');
	$celular   		=		Input::get('celular');

	$mensaje = ' ... marca: '.$marca.' ... modelo: '.$modelo.' ... anio: '.$anio.' ... valor_actual: '.$valor_actual.' ... uso: '.$uso.' ... tipo: '.$tipo.' ... nombre: '.$nombre.' ... celular: '.$celular.' ... email: '.$email.' ... ubicacion: '.$ubicacion;
	$receptorEmail = Input::get('email');
	
	$nombreCliente = $nombre;

    Mail::send('emails.default', array('mensaje'=>$mensaje, 'nombreCliente'=>$nombreCliente), function($message) use ($emisorEmail,$emisorName, $receptorEmail, $subjectFixed)
    {
        $message->from($emisorEmail, $emisorName);
        $message->to($receptorEmail);
        $message->subject($subjectFixed);
    });



	return 'exito';

}));


Route::get('/enviar-correo-prueba', function(){

		// if ( Input::get('email') != 'sergiosaavedratorres@gmail.com' ) {
		// 	return 'hola';
		// }

		$emisorEmail 	= 'aseguratuauto@hermes.pe';
		$receptorEmail 	= 'aseguratuauto@hermes.pe';
		$emisorName 	= 'Asegura tu Auto - Cotización Web';
		$subjectFixed   = 'Asegura tu Auto - Cotización Web';
		$mensaje 		= 'Defecto';

		$marca   	=		Input::get('marca');
		$marca2    	= 		Input::get('marca2');
		$modelo   	=		Input::get('modelo');
		$modelo2   	=		Input::get('modelo2');
		$anio   	=		Input::get('anio');
		$anio2   	=		Input::get('anio2');
		$valor   	=		Input::get('valor');
		$uso   		=		Input::get('uso');
		$uso2   	=		Input::get('uso2');
		$name   	=		Input::get('nombre');
		$fono   	=		Input::get('fono');
		$email   	=		Input::get('email');

		$mensaje = ' ... marca: '.$marca.' ... modelo: '.$modelo.' ... modelo2: '.$modelo2.' ... anio: '.$anio.' ... anio2: '.$anio2.' ... valor: '.$valor.' ... uso: '.$uso.' ... uso2: '.$uso2.' ... name: '.$name.' ... fono: '.$fono.' ... email: '.$email;
		$receptorEmail = Input::get('email');
		
		$nombreCliente = $name;

	    Mail::send('emails.default', array('mensaje'=>$mensaje, 'nombreCliente'=>$nombreCliente), function($message) use ($emisorEmail,$emisorName, $receptorEmail, $subjectFixed)
	    {
	        $message->from($emisorEmail, $emisorName);
	        $message->to($receptorEmail);
	        $message->subject($subjectFixed);
	    });

	    return 'correo enviado';


});


Route::get('/enviar-pdf', function(){

		$emisorEmail 	= 'aseguratuauto@hermes.pe';
		$receptorEmail 	= 'aseguratuauto@hermes.pe';
		$emisorName 	= 'Asegura tu Auto - Cotización Web';
		$subjectFixed   = 'Asegura tu Auto - Cotización Web';
		$mensaje 		= 'Defecto';

		$receptorEmail = 'sergiosaavedratorres@gmail.com';
		
		$nombreCliente = 'Sergio Saavedra';

		$file_pdf = 'pdf-ejemplo.pdf';

	    Mail::send('emails.default', array('mensaje'=>$mensaje, 'nombreCliente'=>$nombreCliente), function($message) use ($emisorEmail,$emisorName, $receptorEmail, $subjectFixed,$file_pdf)
	    {
	        $message->from($emisorEmail, $emisorName)
	        		->to($receptorEmail)
	        		->subject($subjectFixed)
	        		->attach($file_pdf);
	    });

	    return 'correo enviado';
});


Route::get('login', 'AuthController@showLogin');
Route::post('login', 'AuthController@postLogin'); 


// route::get('crear-usuario', function(){

// 	$user = new User();
// 	$user->first_name = 'Admin';
// 	$user->last_name = 'Admin';
// 	$user->username = 'aseguratuauto';
// 	$user->email = 'admin@aseguratuauto.pe';
// 	$user->password = Hash::make('aseguratuauto');
// 	$user->rol_id = 1;
// 	$user->save();

// });


 
Route::group(['before' => 'auth'], function()
{

    Route::get('/admin', array('as' => 'admin.showWelcome', 'uses' => 'HomeController@showWelcome'));
    Route::get('logout', 'AuthController@logOut');
	Route::get('admin/image', array('as' => 'image', 'uses' => 'HomeController@images'));
	Route::get('admin/image/new', array('as' => 'new.image', 'uses' => 'HomeController@new_image'));
	Route::post('admin/image/add', array('as' => 'add.image', 'uses' => 'AdminController@add_image'));
	Route::get('admin/pdf', array('as' => 'pdf', 'uses' => 'HomeController@pdf'));
	Route::get('admin/pdf/new', array('as' => 'new.pdf', 'uses' => 'HomeController@new_pdf'));
	Route::post('admin/pdf/add', array('as' => 'add.pdf', 'uses' => 'AdminController@add_pdf'));
	Route::get('admin/pregunta', array('as' => 'pregunta', 'uses' => 'HomeController@preguntas'));
	Route::get('admin/pregunta/new', array('as' => 'new.pregunta', 'uses' => 'HomeController@new_pregunta'));
	Route::post('admin/pregunta/add', array('as' => 'add.pregunta', 'uses' => 'AdminController@add_pregunta'));
	Route::get('admin/marca', array('as' => 'marca', 'uses' => 'HomeController@marcas'));
	Route::get('admin/marca/new', array('as' => 'new.marca', 'uses' => 'HomeController@new_marca'));
	Route::post('admin/marca/add', array('as' => 'add.marca', 'uses' => 'AdminController@add_marca'));
	Route::get('admin/seguro', array('as' => 'seguro', 'uses' => 'HomeController@seguros'));
	Route::get('admin/seguro/new', array('as' => 'new.seguro', 'uses' => 'HomeController@new_seguro'));
	Route::post('admin/seguro/add', array('as' => 'add.seguro', 'uses' => 'AdminController@add_seguro'));

});