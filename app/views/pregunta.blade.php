@extends('dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div class="col-md-8"> 
                                    <div class="col-md-3">
                                        <h4 class="title">Preguntas Frecuentes</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <a href="{{route('new.pregunta')}}" class="btn btn-info">Agregar <span class="ti-plus"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th width="50">Orden</th>
                                        <th width="240">Titulo</th>
                                        <th>Contenido</th>
                                        <th>Acción</th>
                                    </thead>
                                    <tbody>
                                        <?php $preguntas = Pregunta::orderBy('orden','asc')->get(); ?> 
                                        @foreach($preguntas as $pregunta)
                                        <tr>
                                            <td>{{ $pregunta->orden }}</td>
                                            <td>{{ $pregunta->titulo }}</td>
                                            <td>{{ $pregunta->contenido }}</td>
                                            <td><a href="{{route('new.pregunta','pregunta_id='.$pregunta->id)}}" class="btn btn-info">Editar</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>

                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </footer>

    @stop