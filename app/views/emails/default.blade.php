<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head></head><body bgcolor="FFFFFF" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>  <td align="center" valign="top" bgcolor="ffffff" style="background-color: #ffffff;">
    <div class="preview__panel" id="device" data-device="laptop">
      <!-- CENTERED -->
      <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tbody>
          <tr>
            <td style="font-size:0px">&nbsp;</td>
            <td width="610" align="center" style="font-size:0px;" id="view-content">

              <!-- BEGIN CONTAINER -->
              <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-size: 10px; font-family: Verdana, Helvetica, sans-serif;" id="emailing">
                <tbody>

                  <tr><!-- HEADER -->
                    <td id="header" style="display: table-cell;">
                      <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tbody>
                          <tr>
                            <td>

                              <table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                                <tbody>
                                  <tr>
                                    <td align="left" width="230">
                                      <img src="http://aseguratuauto.pe/nuevo/static-app/img/logo-background.png" border="0" style="display: block; width:100%; vertical-align: top;width: 230px;border-top: 20px solid #FFF;" class="base64 data">
                                    </td>

                                    <td style="font-size:0px">
                                      &nbsp;
                                    </td>

                                    <td align="right" width="230">
                                    </td>            
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #ff6147;font-size: 25px; font-family:Arial; padding:20px;">
                              Hola {{$nombreCliente}}
                            </td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>

                    <tr><!-- CUERPO -->
                      <td id="cuerpo">
                        <table cellpadding="0" cellspacing="0" class="contenedor" align="center">
                          <tbody>
                            <tr>
                              <td align="left" valign="top" style="font-size:16px; font-family: Arial,Helvetica Neue,Helvetica,sans-serif; color:#5d5d5d;">
                                <p style="padding:10px 20px; margin:0;">
                                  Gracias por ingresar tus datos, estamos trabajando en cotizar las mejores opciones de seguro para su veh&iacute;culo. Uno de nuestros ejecutivos se comunicar&aacute; cuanto antes con usted.</p><p style="padding:10px 20px; margin:0;">Ahorra hasta un 35% en tu seguro vehicular sin renunciar a coberturas.</p>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <table cellpadding="0" cellspacing="0" border="0" class="table-responsive" style="
                                  display:inline-block;
                                  width:100%;
                                  vertical-align:top; 
                                  font-size:16px; margin:5px 0px;">
                                  <tbody>
                                    <tr>
                                      <td style="padding: 0 5px;">
                                        <img src="http://aseguratuauto.pe/nuevo/static-app/img/product1.jpg" width="100%" border="0" style="vertical-align: top;">
                                      </td>
                                    </tr>
                                    <!-- <tr>
                                      <td style="padding: 0 5px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                          <tbody>
                                            <tr>
                                              <td bgcolor="2275b9" width="25%" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:48px; font-weight:bold; line-height:1em; color:white; padding:10px">
                                                15%
                                              </td>
                                              <td bgcolor="ff6147" width="100%" align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:white; padding:10px">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec ligula sem. Morbi egestas, turpis sit amet vulputate accumsan, elit metus lacinia lorem, sed sagittis                       
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>                  
                                      </td>
                                    </tr> -->
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                  <tbody>
                                    <tr>
                                      <td width="50%" style="padding:5px">
                                        <img src="http://aseguratuauto.pe/nuevo/static-app/img/Auto2.png" width="100%" border="0" style="vertical-align: top;">
                                      </td>
                                      <td width="50%" style="padding:5px;">
                                        <img src="http://aseguratuauto.pe/nuevo/static-app/img/Auto3.png" width="100%" border="0" style="vertical-align: top;">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>

                    <tr><!-- CIERRE -->
                      <td id="cierre" style="display: table-cell;"></td>
                    </tr>

                    <tr><!-- APP disponible -->
                      <td id="disponible">
                      </td>
                    </tr>

                    <tr>
                      <td style="padding: 0 5px;">
                        <hr style="border-top:1px solid #696969; border-bottom:0px; margin:5px 0;">
                      </td>
                    </tr>

                    <tr><!-- CLAQUETA -->
                      <td id="claqueta" style="display: table-cell;"></td>
                    </tr>

                    <tr><!-- LEGALES -->
                      <td id="legales" style="display: table-cell;">
                        <table width="100%" align="center" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:8px; text-align:justify; color:#7a787b; padding:5px;">
                                T&eacute;rminos y condiciones <br>
                                Este documento tiene carácter informativo, en ningún momento implica cobertura provisional. La cotización es referencial, de existir alguna diferencia con las condiciones de la póliza, prevalecerán las de esta última. Las coberturas aquí descritas están sujetas a las Condiciones Generales y Particulares de la Póliza. Los términos, condiciones y primas aplicables serán especificados en la Póliza que se le entregará al Asegurado al momento de la contratación. El Costo de la instalación y mantenimiento del Dispositivo de Rastreo Satelital (GPS) es totalmente por cuenta del Asegurado. La cartilla de descuentos será entregada junto con la póliza. AseguratuAuto.pe es un servicio de Hermes Corredores de Seguros. Mayor información en: http://www.aseguratuauto.pe El presente correo electrónico se envía conforme a lo dispuesto en la Ley Nro. 28493, modificada por Ley Nro. 29246 y el D.S. 031-05-MTC-Ley Antispam Peruana y su Reglamento. En caso ya no desee recibir este clase de comunicaciones, por favor envíe un correo electrónico a aseguratuauto@hermes.pe con su solicitud de baja para los efectos de futuras publicidades de este servicio.
                              </td>
                            </tr>
                            <tr>
                              <td style="padding: 0 5px;">
                                <hr style="border-top:1px solid #696969; border-bottom:0px; margin:5px 0;">
                              </td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>

                    <tr><!-- DICLAIMER -->
                      <td id="diclaimer" style="display: table-cell;"></td>
                    </tr>

                    <tr><!-- FOOTER -->
                      <td id="footer" style="display: table-cell;"></td>
                    </tr>
                  </tbody>
                </table>

              </td>
              <td style="font-size:0px">&nbsp;</td>
            </tr>
          </tbody>
        </table>            
      </div><!-- / .preview__panel -->
    </td></tr>
  </table></body></html>