@extends('dashboard')

@section('admin-content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="header">
                    <h4 class="title">Agregar Marca</h4>
                </div>
                <div class="content">
                    <form class="multi-page-form" method="POST" action="{{route('add.marca') }} " enctype="multipart/form-data">
                        @if($marca)
                        <input type="hidden" name="marca_id" value="{{$marca->id}}">
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Marca</label>
                                    @if($marca)
                                    <input type="text" name="marca" class="form-control border-input" placeholder="Agregar marca" value="{{$marca->nombre}}">
                                    @else
                                    <input type="text" name="marca" class="form-control border-input" placeholder="Agregar marca">
                                    @endif

                                </div>
                            </div>
                        </div>
                        @if($marca)
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Seo</label>
                                    <input type="text" name="seo" class="form-control border-input" placeholder="Agregar seo" value="{{ $marca->seo }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>¿Se muestra en el footer?</label>
                                    <select name="mostrar" class="form-control">
                                        @if($marca)
                                        @if($marca->status == 0)
                                        <option value="0" selected="true">No se muestra</option>
                                        <option value="1">Sí se muestra</option>
                                        @else
                                        <option value="0">No se muestra</option>
                                        <option value="1" selected="true">Sí se muestra</option>
                                        @endif
                                        @else
                                        <option value="0">No se muestra</option>
                                        <option value="1" >Sí se muestra</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <label>Descripcion</label>
                                @if($marca)
                                <textarea rows="5" name="descripcion" class="form-control border-input" placeholder="Agregar descripcion de la marca">{{$marca->descripcion}}</textarea>
                                @else
                                <textarea rows="5" name="descripcion" class="form-control border-input" placeholder="Agregar descripcion de la marca"></textarea>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="solid col-md-6" for="imagen_marca" id="agregar" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">Marca</label>
                                                <input type="file" id="imagen_marca" style="display: none" name="imagen_marca">
                                            </div>
                                            @if($marca)
                                            <div  class="form-group" style="border: 2px solid #32c5d2;padding:20px;text-align: center;">
                                                <img for="imagen_marca" src="{{asset('/static-app/img/'. $marca->imagen_marca)}}"  style="width: 100%;max-width: 300px;" id="image_marca">
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="solid col-md-6" for="imagen_auto" id="agregar" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">Auto</label>
                                                <input type="file" id="imagen_auto" style="display: none" name="imagen_auto">
                                            </div>
                                            @if($marca)
                                            <div  class="form-group" for="imagen_auto" style="border: 2px solid #32c5d2;padding:20px;text-align: center;">
                                                <img src="{{ asset('/static-app/img/'.$marca->imagen_auto)}}" style="width: 100%;max-width: 300px;" id="image_auto">
                                                </div>
                                            @endif
                                        </div>  
                                    </div>
                        </div>

                        <br><br>
                        <div class="row" style="text-align:center;margin:auto">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info btn-lg btn-wd">Guardar información</button>
                            </div>
                        </div>
                        @if($marca)
                        <br><br><br>
                        <div class="row" style="text-align:center;margin:auto">
                            <div class="col-md-12">
                                <a href="{{route('marca','marca_id='.$marca->id).'&eliminar=1'}}" class="btn btn-danger btn-wd">Eliminar Marca</a>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="http://www.creative-tim.com">
                        Aseguratuauto
                    </a>
                </li>
                <li>
                    <a href="http://blog.creative-tim.com">
                     Blog
                 </a>
             </li>
             <li>
                <a href="http://www.creative-tim.com/license">
                    Licenses
                </a>
            </li>
        </ul>
    </nav>
</div>
</footer>
@stop @section('js1')
    <script>
        $("#imagen_auto").change(function(){

            let file = $("#imagen_auto").val().substr(12);

            $("#agregar[for='imagen_auto']").html(file);

            if(file == ""){
                $("#imagen_auto[for='imagen_auto']").html("Agregar PDF");
            }
        });

        $("#imagen_marca").change(function(){

            let file = $("#imagen_marca").val().substr(12);

            $("#agregar[for='imagen_marca']").html(file);

            if(file == ""){
                $("#imagen_marca[for='imagen_marca']").html("Agregar PDF");
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image_marca').attr('src', e.target.result);
                    $('#image_marca').width(250); 
                    $('#image_marca').height(250);
                    $('#image_marca').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imagen_marca").change(function() {
            readURL(this);
        });
    </script>
@stop
