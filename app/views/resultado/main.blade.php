@extends('layout.master')


@section('content')
<section class="resultado">
    <div class="desktop">
        <table width="100%">
            <tbody><tr>
                <td colspan="7">
                    <h1 class="titulo-azul">
                        <span style="font-size: 27px !important;">COTIZACIÓN SEGURO VEHICULAR:</span>
                        <span style="font-size: 26px !important; text-transform: uppercase; margin-top: -1%;" class="subtitulo-azul-grande">Kia Rio  1.4  2017- 0 Km $13,990</span>
                    </h1>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <table style="width:100%">
                        <tbody><tr>
                            <td style="width:55%">
                                <h3 style="font-size:18px!important;font-family: 'Open Sans';">Cotización # 170966 Uso Particular / Lima</h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr style="height:7px;">
            <td colspan="7"></td>
        </tr>

        <tr style="background-color: #ECF1F7; text-align: center; color: #013465; height: 62px; border-color: #ECF1F7; font-weight: 900;">
            <td style=" border-top-left-radius: 50px; padding-left: 2%;" width="9%">
                <p>Aseguradoras</p>
            </td>
            <td width="10%">
                <p>
                Prima Anual <br>
                </p>
            </td>
            <td width="6%">
                <p>GPS<br></p>
                </td>
                <td width="28%">
                    <p>
                        Coberturas y Beneficios<br>
                    </p>
                </td>
                <td style="border-top-right-radius: 50px;" width="31%">
                    <p>
                        Deducibles<br>
                    </p>
                </td>
            </tr>

            <tr>
                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;">
                    <img src="https://www.ategia.pe/assets/images/peru/hdi.png" alt="HDI Seguros" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;" id="img_HDI Seguros"><a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31667,&quot;677&quot;,31667)"></a>
                    <button class="boton-comun-rosado" style="padding: 5%;margin-top: 8%;">
                        <a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31667,&quot;677&quot;,31667)">Comprar</a>
                    </button>
                </td>
                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;">
                    <h3><b>$677</b></h3>
                    <h5><b>Deducible S/1,100</b></h5>
                    <h6>4 Cuotas s.i. $170</h6>
                    <h6>6 Cuotas s.i. (cargo t. crédito) $113</h6>
                    <h6>10 Cuotas c.i. $74</h6>
                </td>
                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$199.00)</p>
                </td>
                <td style="text-align:left; vertical-align:top ; text-align: left!important; vertical-align: top; border: 1px solid #F2F2F2; background-color: #ECF1F7; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">
                    <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                        <li>Daño Propio: Valor Convenido</li>
                        <li>Resp. Civil a 3ros: Hasta S/450,000</li>
                        <li>Chofer de reemplazo: 3 por año</li>
                    </ul>
                </td>
                <td style="text-align:left; vertical-align:top; text-align: left!important; vertical-align: top; background-color: #ECF1F7; color: #013465; height: 90px; border-color: #ECF1F7; font-weight: 900;">
                    <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                        <li>Taller Multimarca/Preferente: S/1,100</li>
                        <li>Taller afiliado S/1,100</li>
                        <li>Taller no afiliado S/1,100</li>
                    </ul>

                </td>
            </tr>
            <tr class="RowComprar" id="ComprarCia_31667" style="display:none;">
                <td colspan="7">
                    <table style="width:100%;height:126px;">
                        <tbody><tr>
                            <td style="width:33%; text-align: center;">
                                <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button></td>
                                <td style="width:34%; text-align: center;">
                                    <div id="dvLlamanos_btn_6">
                                        <button type="button" id="btnLlamanos_6" onclick="MostrarTelefono(6)" class="btn btn-danger" style="background-color: #F18626; border-color: #F18626; font-size: 14px; cursor: pointer">LLÁMANOS</button>
                                    </div>
                                    <div style="display:none;" class="fonoCotizador" id="dvLlamanos_fono_6">(01) 207-6170</div></td>
                                    <td style="width:33%; text-align: center;">
                                        <button type="button" class="btn btn-danger btnTeLlamamos" style="background-color: #01B1AE; border-color: #01B1AE; font-size: 14px; cursor: pointer">TE LLAMAMOS</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;">
                        <img src="https://www.ategia.pe/assets/images/peru/rimac.png" alt="Rimac" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;" id="img_Rimac"><a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31268,&quot;715&quot;,31268)"></a>
                        <button class="boton-comun-rosado" style="padding: 5%;margin-top: 8%;">
                            <a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31268,&quot;715&quot;,31268)">Comprar</a>
                        </button>
                    </td>
                    <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;">
                        <h3><b>$715</b></h3>
                        <h5><b>Vehículo aplica a campaña C.5</b></h5>
                        <h6>4 Cuotas s.i. $179</h6>
                        <h6>6 Cuotas s.i. (cargo t. crédito) $120</h6>
                        <h6>10 Cuotas c.i. $79</h6>
                        <h6>10 Cuotas s.i. $72</h6>
                    </td>
                    <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$177.00)</p>
                    </td>
                    <td style="text-align:left; vertical-align:top ; text-align: left!important; vertical-align: top; border: 1px solid #F2F2F2; background-color: #ECF1F7; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">
                        <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                            <li>Daño Propio: Valor Convenido</li>
                            <li>Resp. Civil a 3ros: Hasta $150,000</li>
                            <li>Chofer de reemplazo: 3 por año</li>
                        </ul>
                    </td>
                    <td style="text-align:left; vertical-align:top; text-align: left!important; vertical-align: top; background-color: #ECF1F7; color: #013465; height: 90px; border-color: #ECF1F7; font-weight: 900;">
                        <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                            <li>Taller Multimarca/Preferente: 15%, MIN $150</li>
                            <li>Taller afiliado 20%, MIN $200</li>
                            <li>Taller no afiliado Plan no aplica</li>
                        </ul>

                    </td>
                </tr>
                <tr class="RowComprar" id="ComprarCia_31268" style="display:none;">
                    <td colspan="7">
                        <table style="width:100%;height:126px;">
                            <tbody><tr>
                                <td style="width:33%; text-align: center;">
                                    <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button></td>
                                    <td style="width:34%; text-align: center;">
                                        <div id="dvLlamanos_btn_1">
                                            <button type="button" id="btnLlamanos_1" onclick="MostrarTelefono(1)" class="btn btn-danger" style="background-color: #F18626; border-color: #F18626; font-size: 14px; cursor: pointer">LLÁMANOS</button>
                                        </div>
                                        <div style="display:none;" class="fonoCotizador" id="dvLlamanos_fono_1">(01) 207-6170</div></td>
                                        <td style="width:33%; text-align: center;">
                                            <button type="button" class="btn btn-danger btnTeLlamamos" style="background-color: #01B1AE; border-color: #01B1AE; font-size: 14px; cursor: pointer">TE LLAMAMOS</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;">
                            <img src="https://www.ategia.pe/assets/images/peru/positiva.png" alt="La Positiva" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;" id="img_La Positiva"><a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31259,&quot;766&quot;,31259)"></a>
                            <button class="boton-comun-rosado" style="padding: 5%;margin-top: 8%;">
                                <a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31259,&quot;766&quot;,31259)">Comprar</a>
                            </button>
                        </td>
                        <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;">
                            <h3><b>$766</b></h3>
                            <h6>4 Cuotas s.i. $192</h6>
                            <h6>10 Cuotas c.i. $85</h6>
                        </td>

                        <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$177.00)</p>
                        </td>
                        <td style="text-align:left; vertical-align:top ; text-align: left!important; vertical-align: top; border: 1px solid #F2F2F2; background-color: #ECF1F7; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">
                            <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                                <li>Daño Propio: Valor Asegurado</li>
                                <li>Resp. Civil a 3ros: Hasta $150,000</li>
                                <li>Chofer de reemplazo: 3 por año</li>
                            </ul>
                        </td>
                        <td style="text-align:left; vertical-align:top; text-align: left!important; vertical-align: top; background-color: #ECF1F7; color: #013465; height: 90px; border-color: #ECF1F7; font-weight: 900;">
                            <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                                <li>Taller Multimarca/Preferente: 10%, MIN $150 / 12%, MIN $150</li>
                                <li>Taller afiliado 15%, MIN $250</li>
                                <li>Taller no afiliado 20%, MIN $300</li>
                            </ul>

                        </td>
                    </tr>
                    <tr class="RowComprar" id="ComprarCia_31259" style="display:none;">
                        <td colspan="7">
                            <table style="width:100%;height:126px;">
                                <tbody><tr>
                                    <td style="width:33%; text-align: center;">
                                        <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button></td>
                                        <td style="width:34%; text-align: center;">
                                            <div id="dvLlamanos_btn_4">
                                                <button type="button" id="btnLlamanos_4" onclick="MostrarTelefono(4)" class="btn btn-danger" style="background-color: #F18626; border-color: #F18626; font-size: 14px; cursor: pointer">LLÁMANOS</button>
                                            </div>
                                            <div style="display:none;" class="fonoCotizador" id="dvLlamanos_fono_4">(01) 207-6170</div></td>
                                            <td style="width:33%; text-align: center;">
                                                <button type="button" class="btn btn-danger btnTeLlamamos" style="background-color: #01B1AE; border-color: #01B1AE; font-size: 14px; cursor: pointer">TE LLAMAMOS</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;">
                                <img src="https://www.ategia.pe/assets/images/peru/pacifico.png" alt="Pacífico" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;" id="img_Pacífico"><a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,32511,&quot;781&quot;,32511)"></a>
                                <button class="boton-comun-rosado" style="padding: 5%;margin-top: 8%;">
                                    <a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,32511,&quot;781&quot;,32511)">Comprar</a>
                                </button>
                            </td>
                            <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;">
                                <h3><b>$781</b></h3>
                                <h6>4 Cuotas s.i. $196</h6>
                                <h6>6 Cuotas s.i. (cargo t. crédito) $131</h6>
                                <h6>10 Cuotas c.i. $85</h6>
                            </td>

                            <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$177.00)</p>
                            </td>
                            <td style="text-align:left; vertical-align:top ; text-align: left!important; vertical-align: top; border: 1px solid #F2F2F2; background-color: #ECF1F7; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">
                                <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                                    <li>Daño Propio: Suma Asegurada</li>
                                    <li>Resp. Civil a 3ros: Hasta $150,000</li>
                                    <li>Chofer de reemplazo: 3 al año sin costo / $25 adicionales</li>
                                </ul>
                            </td>
                            <td style="text-align:left; vertical-align:top; text-align: left!important; vertical-align: top; background-color: #ECF1F7; color: #013465; height: 90px; border-color: #ECF1F7; font-weight: 900;">
                                <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                                    <li>Taller Multimarca/Preferente: 10% MIN $150</li>
                                    <li>Taller afiliado RED 1 = 15%, MIN $200 / RED 2 = 20%, MIN $200</li>
                                    <li>Taller no afiliado 25%, MIN $350</li>
                                </ul>

                            </td>
                        </tr>
                        <tr class="RowComprar" id="ComprarCia_32511" style="display:none;">
                            <td colspan="7">
                                <table style="width:100%;height:126px;">
                                    <tbody><tr>
                                        <td style="width:33%; text-align: center;">
                                            <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button></td>
                                            <td style="width:34%; text-align: center;">
                                                <div id="dvLlamanos_btn_2">
                                                    <button type="button" id="btnLlamanos_2" onclick="MostrarTelefono(2)" class="btn btn-danger" style="background-color: #F18626; border-color: #F18626; font-size: 14px; cursor: pointer">LLÁMANOS</button>
                                                </div>
                                                <div style="display:none;" class="fonoCotizador" id="dvLlamanos_fono_2">(01) 207-6170</div></td>
                                                <td style="width:33%; text-align: center;">
                                                    <button type="button" class="btn btn-danger btnTeLlamamos" style="background-color: #01B1AE; border-color: #01B1AE; font-size: 14px; cursor: pointer">TE LLAMAMOS</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465; border-bottom-left-radius: 50px!important;">
                                    <img src="https://www.ategia.pe/assets/images/peru/mapfre.png" alt="Mapfre" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;" id="img_Mapfre"><a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31353,&quot;1463&quot;,31353)"></a>
                                    <button class="boton-comun-rosado" style="padding: 5%;margin-top: 8%;">
                                        <a class="hover-none" style="color:#fff;" href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31353,&quot;1463&quot;,31353)">Comprar</a>
                                    </button>
                                </td>
                                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;">
                                    <h3><b>$1,463</b></h3>
                                    <h5><b>Póliza 2 años</b></h5>
                                    <h6>4 Cuotas s.i. $366</h6>
                                    <h6>10 Cuotas c.i. $161</h6>
                                </td>

                                <td style="border-bottom-right-radius: 50px!important; text-align:center; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$207.00)</p>
                                </td>
                                <td style="text-align:left; vertical-align:top ; text-align: left!important; vertical-align: top; border: 1px solid #F2F2F2; background-color: #ECF1F7; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">
                                    <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                                        <li>Daño Propio: Valor Asegurado (2 años)</li>
                                        <li>Resp. Civil a 3ros: Hasta $150,000</li>
                                        <li>Chofer de reemplazo: 5 por año (Co pago s/.20.00)</li>
                                    </ul>
                                </td>
                                <td style="border-bottom-right-radius: 28px!important; text-align:left; vertical-align:top; text-align: left!important; vertical-align: top; background-color: #ECF1F7; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">
                                    <ul class="ItemsCoberturasDeducibles" style="list-style:none;">
                                        <li>Taller Multimarca/Preferente: 15%, MIN $150</li>
                                        <li>Taller afiliado RED 1 = 20%, MIN $200 / RED 2 = 20%, MIN $300</li>
                                        <li>Taller no afiliado 20%, MIN $300</li>
                                    </ul>

                                </td>
                            </tr>
                            <tr class="RowComprar" id="ComprarCia_31353" style="display:none;">
                                <td colspan="7">
                                    <table style="width:100%;height:126px;">
                                        <tbody><tr>
                                            <td style="width:33%; text-align: center;">
                                                <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button></td>
                                                <td style="width:34%; text-align: center;">
                                                    <div id="dvLlamanos_btn_3">
                                                        <button type="button" id="btnLlamanos_3" onclick="MostrarTelefono(3)" class="btn btn-danger" style="background-color: #F18626; border-color: #F18626; font-size: 14px; cursor: pointer">LLÁMANOS</button>
                                                    </div>
                                                    <div style="display:none;" class="fonoCotizador" id="dvLlamanos_fono_3">(01) 207-6170</div></td>
                                                    <td style="width:33%; text-align: center;">
                                                        <button type="button" class="btn btn-danger btnTeLlamamos" style="background-color: #01B1AE; border-color: #01B1AE; font-size: 14px; cursor: pointer">TE LLAMAMOS</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="mobile">
                        <table width="100%">
                            <tbody><tr style="color: rgb(0,22,87); text-align: center; height: 60px;">
                                <td width="30%"></td>
                                <td width="30%">
                                    Prima Anual
                                </td>
                                <td width="28%">
                                    Tarjeta<br>Regalo
                                </td>
                                <td width="14%">
                                    GPS
                                </td>
                            </tr>
                            <tr style="color: rgb(0,22,87); text-align: center;">
                                <td width="30%"></td>
                                <td width="30%">
                                    <p class="border_bajo"></p>
                                </td>
                                <td width="28%">
                                    <p class="border_bajo"></p>
                                </td>
                                <td width="14%">
                                    <p class="border_bajo"></p>
                                </td>
                            </tr>
                            <tr onclick="IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31667,&quot;677&quot;,6);ContinuarProcesoOnLine();">
                                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;height: 5px;">
                                    <img src="https://www.ategia.pe/assets/images/peru/hdi.png" alt="HDI Seguros" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;width: 50%;" id="img_HDI Seguros"><a href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31667,&quot;677&quot;,31667)">
                                    <img src="https://www.segurosimple.com/assets/images/boton-compra-mobile.png?version=123" style="width:40%;cursor:pointer;"></a>
                                </td>
                                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;"><h3>$677</h3>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;"><h4 style="font-weight: 900;">$30</h4>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$199.00)</p>
                                </td>
                            </tr>
                            <tr class="RowComprar" id="ComprarCia_31667" style="display:none;text-align: center;">
                                <td colspan="4">
                                    <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button>
                                </td>
                            </tr>
                            <tr onclick="IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31268,&quot;715&quot;,1);ContinuarProcesoOnLine();">
                                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;height: 5px;">
                                    <img src="https://www.ategia.pe/assets/images/peru/rimac.png" alt="Rimac" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;width: 50%;" id="img_Rimac"><a href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31268,&quot;715&quot;,31268)">
                                    <img src="https://www.segurosimple.com/assets/images/boton-compra-mobile.png?version=123" style="width:40%;cursor:pointer;"></a>
                                </td>
                                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;"><h3>$715</h3>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;"><h4 style="font-weight: 900;">$30</h4>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$177.00)</p>
                                </td>
                            </tr>
                            <tr class="RowComprar" id="ComprarCia_31268" style="display:none;text-align: center;">
                                <td colspan="4">
                                    <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button>
                                </td>
                            </tr>
                            <tr onclick="IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31259,&quot;766&quot;,4);ContinuarProcesoOnLine();">
                                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;height: 5px;">
                                    <img src="https://www.ategia.pe/assets/images/peru/positiva.png" alt="La Positiva" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;width: 50%;" id="img_La Positiva"><a href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31259,&quot;766&quot;,31259)">
                                    <img src="https://www.segurosimple.com/assets/images/boton-compra-mobile.png?version=123" style="width:40%;cursor:pointer;"></a>
                                </td>
                                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;"><h3>$766</h3>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;"><h4 style="font-weight: 900;">$40</h4>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$177.00)</p>
                                </td>
                            </tr>
                            <tr class="RowComprar" id="ComprarCia_31259" style="display:none;text-align: center;">
                                <td colspan="4">
                                    <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button>
                                </td>
                            </tr>
                            <tr onclick="IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,32511,&quot;781&quot;,2);ContinuarProcesoOnLine();">
                                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465;height: 5px;">
                                    <img src="https://www.ategia.pe/assets/images/peru/pacifico.png" alt="Pacífico" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;width: 50%;" id="img_Pacífico"><a href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,32511,&quot;781&quot;,32511)">
                                    <img src="https://www.segurosimple.com/assets/images/boton-compra-mobile.png?version=123" style="width:40%;cursor:pointer;"></a>
                                </td>
                                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;"><h3>$781</h3>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;"><h4 style="font-weight: 900;">$40</h4>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$177.00)</p>
                                </td>
                            </tr>
                            <tr class="RowComprar" id="ComprarCia_32511" style="display:none;text-align: center;">
                                <td colspan="4">
                                    <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button>
                                </td>
                            </tr>
                            <tr onclick="IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31353,&quot;1463&quot;,3);ContinuarProcesoOnLine();">
                                <td style="text-align:center; background-color: #ECF1F7; border-color: #ECF1F7; color: #013465; border-bottom-left-radius: 50px!important;height: 5px;">
                                    <img src="https://www.ategia.pe/assets/images/peru/mapfre.png" alt="Mapfre" style="line-height: 10px;border-radius:&nbsp;5px; width:80%;width: 50%;" id="img_Mapfre"><a href="#" onclick="javascript:IraSolicitudDesdeCotizacion(1,&quot;00003&quot;,31353,&quot;1463&quot;,31353)">
                                    <img src="https://www.segurosimple.com/assets/images/boton-compra-mobile.png?version=123" style="width:40%;cursor:pointer;"></a>
                                </td>
                                <td style="text-align:center;border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900; padding-left: 1%; padding-right: 1%;"><h3>$1,463</h3>
                                </td>
                                <td style="text-align:center; border: 1px solid #F2F2F2; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;"><h4 style="font-weight: 900;">$30</h4>
                                </td>
                                <td style="border-bottom-right-radius: 50px!important; text-align:center; background-color: #ECF1F7; text-align: center; color: #013465; height: 5px; border-color: #ECF1F7; font-weight: 900;">Sí<br><p class="refPrecioGPS">(Aprox.<br>$207.00)</p>
                                </td>
                            </tr>
                            <tr class="RowComprar" id="ComprarCia_31353" style="display:none;text-align: center;">
                                <td colspan="4">
                                    <button onclick="ContinuarProcesoOnLine();" type="button" class="btn btn-danger" style="background-color: #A4348A; border-color: #A4348A; font-size: 14px; cursor: pointer">CONTRATAR SEGURO ONLINE</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            @stop