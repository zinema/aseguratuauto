@extends('dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div class="col-md-8"> 
                                    <div class="col-md-3">
                                        <h4 class="title">Archivos Pdf</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <a href="{{route('new.pdf')}}" class="btn btn-info">Agregar <span class="ti-plus"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Titulo</th>
                                        <th>File</th>
                                    </thead>
                                    <tbody>
                                        <?php $pdfs = Pdf::all() ?>
                                        @foreach($pdfs as $pdf)
                                            <tr>
                                                <td>{{ $pdf->id }}</td>
                                                <td>{{ $pdf->original_title }}</td>
                                                <td>
                                                    <a href="{{ asset( $pdf->title ) }}" target="_blank" class="btn btn-call-us btn-cartilla">
                                                        Descargar
                                                    </a>
                                                </td>
                                                <td><a href="{{route('new.pdf','pdf_id='.$pdf->id)}}" class="btn btn-info">Editar</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>  
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>

                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </footer>

    @stop