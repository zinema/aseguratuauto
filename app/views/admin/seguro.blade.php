@extends('admin.dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div class="col-md-8"> 
                                    <div class="col-md-2">
                                        <h4 class="title">Seguro</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <a href="{{route('new.seguro')}}" class="btn btn-info">Agregar <span class="ti-plus"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Seo</th>
                                        <th>Logo</th>
                                        <th>Descripcion</th>
                                        <th>Status</th>
                                        <th>Acción</th>
                                    </thead>
                                    <tbody>
                                        <?php $seguros = Seguro::all(); ?>
                                        @foreach($seguros as $seguro)
                                            <tr>
                                                <td>{{ $seguro->id }}</td>
                                                <td>{{ $seguro->nombre }}</td>
                                                <td>{{ $seguro->seo }}</td>
                                                <td><img src="{{asset('uploads/'. $seguro->imagen)}}" style="max-width: 100px;"></td>
                                                <td>{{ $seguro->descripcion }}</td>
                                                <td>
                                                    @if($seguro->status == 1)
                                                      <btn class="btn btn-sm btn-success btn-icon"><i class="fa fa-check"></i></btn>
                                                    @else
                                                      <btn class="btn btn-sm btn-danger btn-icon"><i class="fa fa-exclamation"></i></btn>
                                                    @endif
                                                </td>
                                                <td><a href="{{route('new.seguro','seguro_id='.$seguro->id)}}" class="btn btn-info">Editar</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>  
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>

                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </footer>

    @stop