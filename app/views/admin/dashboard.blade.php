<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Aseguratuauto CMS</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <link href="{{ asset('static-app/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('static-app/assets/css/animate.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('static-app/assets/css/paper-dashboard.css')}}" rel="stylesheet"/>
    <link href="{{asset('static-app/assets/css/demo.css')}}" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('static-app/assets/css/themify-icons.css')}}" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Aseguratuauto
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{ route('admin.showWelcome') }}">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('image')}}">
                        <i class="ti-gallery"></i>
                        <p>Cargar imagen</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('pdf')}}">
                        <i class="ti-download"></i>
                        <p>Cargar PDF</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('pregunta')}}">
                        <i class="ti-comment-alt"></i>
                        <p>Preguntas</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('marca')}}">
                        <i class="ti-check-box"></i>
                        <p>Marcas</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('seguro')}}">
                        <i class="ti-heart"></i>
                        <p>Seguros</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <!-- <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
                                <p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
                                    <p>Notifications</p>
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="ti-settings"></i>
                                <p>Settings</p>
                            </a>
                        </li>
                    </ul>
                </div> -->
            </div>
        </nav>
    @yield('admin-content')
    </div>
    
</div>


</body>

    <script src="{{ asset('static-app/assets/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
    <script src="{{ asset('static-app/assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ asset('static-app/assets/js/bootstrap-checkbox-radio.js')}}"></script>

    <!--  Charts Plugin -->
    <script src="{{ asset('static-app/assets/js/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('static-app/assets/js/bootstrap-notify.js')}}"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="{{ asset('static-app/assets/js/paper-dashboard.js')}}"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ asset('static-app/assets/js/demo.js')}}"></script>
    @yield('js1')
	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'ti-user',
            	message: "Bienvenido al <b>CMS</b> de Aseguratuauto."

            },{
                type: 'success',
                timer: 4000
            });

    	});
	</script>

</html>
