@extends('admin.dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                            <div class="header">
                                <h4 class="title">Agregar PDF</h4>
                            </div>
                            <div class="content">
                                <form class="multi-page-form form-horizontal" method="POST" action="{{route('add.pdf') }} " enctype="multipart/form-data">
                                @if($pdf)
                                <input type="hidden" name="pdf_id" value="{{$pdf->id}}">
                                @endif
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="pdf" class="control-label col-md-4">Archivo PDF</label>
                                                @if($pdf)
                                                    <label class="solid col-md-6" for="pdf" id="agregar" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">{{$pdf->original_title}}</label>
                                                @else
                                                    <label class="solid col-md-6" for="pdf" id="agregar" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">Agregar PDF</label>
                                                @endif
                                                <input type="file" id="pdf" style="display: none" name="pdf">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Enviar</button>
                                    </div>
                                    @if($pdf)
                                    <br><br><br>
                                    <div class="row" style="text-align:center;margin:auto">
                                        <div class="col-md-12">
                                            <a href="{{route('pdf','pdf_id='.$pdf->id).'&eliminar=1'}}" class="btn btn-danger btn-wd">Eliminar PDF</a>
                                        </div>
                                    </div>
                                    @endif
                                </form>
                            </div>
                        </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                    <li>
                        <a href="http://www.creative-tim.com/license">
                            Licenses
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </footer>
@stop @section('js1')

    <script>
        $("#pdf").change(function(){

            let file = $("#pdf").val().substr(12);

            $("#agregar[for='pdf']").html(file);

            if(file == ""){
                $("#agregar[for='pdf']").html("Agregar PDF");
            }
        });
    </script>
    @stop