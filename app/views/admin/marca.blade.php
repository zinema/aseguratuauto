@extends('admin.dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div class="col-md-8"> 
                                    <div class="col-md-2">
                                        <h4 class="title">Marca</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <a href="{{route('new.marca')}}" class="btn btn-info">Agregar <span class="ti-plus"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Marca</th>
                                        <th>Seo</th>
                                        <th>Logo</th>
                                        <th>Auto</th>
                                        <th>Descripcion</th>
                                        <th>Estado</th>
                                        <th>Acción</th>
                                    </thead>
                                    <tbody>
                                        <?php $marcas = Marca::all(); ?>
                                        @foreach($marcas as $marca)
                                            <tr>
                                                <td>{{ $marca->id }}</td>
                                                <td>{{ $marca->nombre }}</td>
                                                <td>{{ $marca->seo }}</td>
                                                <td><img src="{{asset('uploads/'. $marca->imagen_marca)}}" style="max-width: 100px;"></td>
                                                <td><img src="{{asset('uploads/'. $marca->imagen_auto)}}" style="max-width: 100px;"></td>
                                                <td>{{ $marca->descripcion }}</td>
                                                <td>
                                                    @if($marca->status == 1)
                                                      <btn class="btn btn-sm btn-success btn-icon"><i class="fa fa-check"></i></btn>
                                                    @else
                                                      <btn class="btn btn-sm btn-danger btn-icon"><i class="fa fa-exclamation"></i></btn>
                                                    @endif
                                                </td>
                                                <td><a href="{{route('new.marca','marca_id='.$marca->id)}}" class="btn btn-info">Editar</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>  
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>

                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </footer>

    @stop