@extends('admin.dashboard')

@section('admin-content')
<style type="text/css">
    #imagen_seguro:hover{
        cursor: pointer;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="header">
                    <h4 class="title">Agregar Seguro</h4>
                </div>
                <div class="content">
                    <form class="multi-page-form" method="POST" action="{{route('add.seguro') }} " enctype="multipart/form-data">
                        @if($seguro)
                        <input type="hidden" name="seguro_id" value="{{$seguro->id}}">
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Seguro</label>
                                    @if($seguro)
                                    <input type="text" name="seguro" class="form-control border-input" placeholder="Agregar seguro" value="{{$seguro->nombre}}">
                                    @else
                                    <input type="text" name="seguro" class="form-control border-input" placeholder="Agregar seguro">
                                    @endif

                                </div>
                            </div>
                        </div>

                        @if($seguro)
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Seo</label>
                                    <input type="text" name="seo" class="form-control border-input" placeholder="Agregar seo" value="{{ $seguro->seo }}">
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>¿Se muestra en el footer?</label>
                                    <select name="mostrar" class="form-control">
                                        @if($seguro)
                                        @if($seguro->status == 0)
                                        <option value="0" selected="true">No se muestra</option>
                                        <option value="1">Sí se muestra</option>
                                        @else
                                        <option value="0">No se muestra</option>
                                        <option value="1" selected="true">Sí se muestra</option>
                                        @endif
                                        @else
                                        <option value="0">No se muestra</option>
                                        <option value="1" >Sí se muestra</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <label>Descripcion</label>
                                @if($seguro)
                                <textarea rows="5" name="descripcion" class="form-control border-input" placeholder="Agregar descripcion de la seguro">{{$seguro->descripcion}}</textarea>
                                @else
                                <textarea rows="5" name="descripcion" class="form-control border-input" placeholder="Agregar descripcion de la seguro"></textarea>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                            <label class="solid col-md-6" for="imagen_seguro" id="agregar" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">Subir Imagen <i class="ti-gallery"></i></label>
                                            <input type="file" id="imagen_seguro" name="imagen_seguro">
                                        @if($seguro)
                                            <img for="imagen_seguro" src="{{asset('/uploads/'. $seguro->imagen)}}" id="image_seguro" style="max-width: 200px !important;">
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                                <label class="solid col-md-6" for="imagen_header" id="agregar" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">Imagen Header <i class="ti-gallery"></i></label>
                                                <input type="file" id="imagen_header" name="imagen_header">
                                            @if($seguro)
                                                <img src="{{ asset('/uploads/'.$seguro->imagen_header)}}" id="imagen_header" style="max-width: 200px !important;">
                                            @endif
                                        </div> 
                                </div>
                            </div>
                        <br><br>
                        <div class="row" style="text-align:center;margin:auto">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info btn-lg btn-wd">Guardar información</button>
                            </div>
                        </div>
                       <!--  @if($seguro)
                        <br><br><br>
                        <div class="row" style="text-align:center;margin:auto">
                            <div class="col-md-12">
                                <a href="{{route('seguro','seguro_id='.$seguro->id).'&eliminar=1'}}" class="btn btn-danger btn-wd">Eliminar seguro</a>
                            </div>
                        </div>
                        @endif -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="http://www.creative-tim.com">
                        Aseguratuauto
                    </a>
                </li>
                <li>
                    <a href="http://blog.creative-tim.com">
                     Blog
                 </a>
             </li>
             <li>
                <a href="http://www.creative-tim.com/license">
                    Licenses
                </a>
            </li>
        </ul>
    </nav>
</div>
</footer>
@stop @section('js1')
    <script>

        $("#imagen_seguro").change(function(){

            let file = $("#imagen_seguro").val().substr(12);

            $("#agregar[for='imagen_seguro']").html(file);

            if(file == ""){
                $("#imagen_seguro[for='imagen_seguro']").html("Agregar PDF");
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image_seguro').attr('src', e.target.result);
                    // $('#image_seguro').width(250); 
                    // $('#image_seguro').height(250);
                    $('#image_seguro').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imagen_seguro").change(function() {
            readURL(this);
        });
    </script>
@stop
