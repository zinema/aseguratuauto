@extends('admin.dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div class="col-md-9"> 
                                    <div class="col-md-2">
                                        <h4 class="title">Imagenes</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <a href="{{route('new.image')}}" class="btn btn-info">Agregar <span class="ti-plus"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Imagen</th>
                                        <th>Estado</th>
                                        <th>Orden</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <?php $images = Imagen::all(); ?>
                                        @foreach($images as $image)
                                        <tr>
                                            <td>{{ $image->id }}</td>
                                            <td><img width="200" height="100" src="{{ asset('uploads/'.$image->image ) }}"></td>
                                            @if($image->status == 1)
                                                <td>Activo</td>
                                            @else
                                                <td>No activo</td>
                                            @endif
                                            <td>{{$image->order}}</td>
                                            <td><a href="{{route('new.image','image_id='.$image->id)}}" class="btn btn-info">Editar</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>  
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>

                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </footer>

    @stop