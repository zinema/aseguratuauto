<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Asegura tu auto</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="canonical" href="#" />
    <meta name="csrf_token" content="<?= csrf_token() ?>">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700,900|Material+Icons" rel="stylesheet">
    <link href="{{asset('static-app/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('static-app/css/material-kit.css')}}" rel="stylesheet"/>
    <link href="{{asset('static-app/css/bootstrapValidator.min.css')}}" rel="stylesheet" type="text/css">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('static-app/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('static-app/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('static-app/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('static-app/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('static-app/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('static-app/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('static-app/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('static-app/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('static-app/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('static-app/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('static-app/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('static-app/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('static-app/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('static-app/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('static-app/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
    <div class="container">
        {{ Form::open(['url' => 'login']) }}
 
            @if(Session::has('error_message'))
                {{ Session::get('error_message') }}
            @endif
 
            <h2>Log in</h2>
 
            {{ Form::label('username', 'Username') }}
            {{ Form::text('username') }}
 
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password') }}
 
            <label>
                {{ Form::checkbox('remember', true) }} Remember me
            </label>
            
            {{ Form::submit('Log in', ['class' => 'btn btn-primary btn-block']) }}
    
        {{ Form::close() }}
    </div>
</body>
</html>



  </div>