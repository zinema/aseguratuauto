@extends('admin.dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Agregar Pregunta</h4>
                    </div>
                    <div class="content">
                        <form class="multi-page-form " method="POST" action="{{route('add.pregunta') }} " enctype="multipart/form-data">
                            @if($pregunta)
                            <input type="hidden" name="pregunta_id" value="{{$pregunta->id}}">
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="orden" class="form-control">
                                            <option value="">Selecciona el orden</option>
                                            @for( $k = 1 ; $k < 21 ; $k++ )
                                                @if($pregunta)
                                                    @if($pregunta->orden == $k)
                                                    <option value="{{$k}}" selected="true">{{$k}}</option>
                                                    @else
                                                    <option value="{{$k}}">{{$k}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$k}}">{{$k}}</option>
                                                @endif
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Pregunta</label>
                                        @if($pregunta)
                                        <input type="text" name="titulo" class="form-control border-input" placeholder="Agregar pregunta" value="{{$pregunta->titulo}}">
                                        @else
                                        <input type="text" name="titulo" class="form-control border-input" placeholder="Agregar pregunta">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contenido</label>
                                        @if($pregunta)
                                        <textarea rows="5" name="contenido" class="form-control border-input" placeholder="Agregar el contenido de la pregunta" >{{$pregunta->contenido}}</textarea>
                                        @else
                                        <textarea rows="5" name="contenido" class="form-control border-input" placeholder="Agregar el contenido de la pregunta" ></textarea>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-lg btn-wd">Enviar</button>
                            </div>
                            <!-- @if($pregunta)
                            <br><br><br>
                            <div class="row" style="margin:auto;text-align: center">
                                <div class="col-md-12">
                                    <a href="{{route('pregunta','pregunta_id='.$pregunta->id).'&eliminar=1'}}" class="btn btn-danger btn-wd">Eliminar Pregunta</a>
                                </div>
                            </div>
                            @endif -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                    <li>
                        <a href="http://www.creative-tim.com/license">
                            Licenses
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </footer>
@stop @section('js1')

    <script>
        $("#pdf").change(function(){

            let file = $("#pdf").val().substr(12);

            $("#agregar[for='pdf']").html(file);

            if(file == ""){
                $("#pdf[for='pdf']").html("Agregar PDF");
            }
        });
    </script>
    @stop