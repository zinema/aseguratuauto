<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="{{asset('asset/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('asset/css/fuentes.css')}}">

	<link rel="apple-touch-icon" sizes="57x57" href="{{asset('asset/fonts/apple-icon-57x57.png')}}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{asset('asset/fonts/apple-icon-60x60.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{asset('asset/fonts/apple-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{asset('asset/fonts/apple-icon-76x76.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{asset('asset/fonts/apple-icon-114x114.png')}}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{asset('asset/fonts/apple-icon-120x120.png')}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{asset('asset/fonts/apple-icon-144x144.png')}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{asset('asset/fonts/apple-icon-152x152.png')}}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('asset/fonts/apple-icon-180x180.png')}}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('asset/fonts/android-icon-192x192.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('asset/fonts/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{asset('asset/fonts/favicon-96x96.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('asset/fonts/favicon-16x16.png')}}">
	<link rel="manifest" href="{{asset('asset/fonts/manifest.json')}}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{asset('asset/fonts/ms-icon-144x144.png')}}">
	<meta name="theme-color" content="#ffffff">


	

	<!-- Integracion Cotizador -->

	<script src="{{asset('asset/js/angular.js')}}" ></script>
	<script src="{{asset('asset/js/ngStorage.js')}}" ></script>
	<script src="{{asset('asset/js/angular-route.js')}}" ></script>
	<script src="{{asset('asset/js/cotizauser.js')}}"></script>

	<!--   End     -->
	


	@section('metas')
	<title>Aseguratuauto | Seguro Vehicular - Seguro de auto o vehículo</title>

	<meta name="description" content="Cotiza y contrata tu seguro vehicular en Aseguratuauto. Ahorra comparando opciones de seguro de autos con todas las aseguradoras del mercado en Perú.">
	<meta name="keywords" content="seguro, seguro vehicular, vehiculo, soat, todo riesgo, rimac, pacificos seguros, HDI, Mapfre, positiva">
	<meta name="author" content="Aseguratuauto.pe">

	<meta itemprop="name" content="Aseguratuauto | Seguro Vehicular - Seguro de auto o vehículo">
	<meta itemprop="description" content="Cotiza y contrata tu seguro vehicular en Aseguratuauto. Ahorra comparando opciones de seguro de autos con todas las aseguradoras del mercado en Perú.">
	<meta itemprop="image" content="{{ asset('asset/img/carro_1920.png') }}">

	<meta property="og:title" content="Aseguratuauto | Seguro Vehicular - Seguro de auto o vehículo" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="{{ asset('asset/img/carro_1920.png') }}" />
	<meta property="og:description" content="Cotiza y contrata tu seguro vehicular en Aseguratuauto. Ahorra comparando opciones de seguro de autos con todas las aseguradoras del mercado en Perú." />
	<meta property="og:site_name" content="Aseguratuauto" />
	@stop

	@yield('metas')
	@yield('css')

</head>
<body>

	@yield('content')

	@include('principal.modulo.como-funciona')

	@include('principal.modulo.experiencia')

	<div id="call2action">
		<div class="container">
			<div class="row">
				<div class="call2action-container col-md-12">
					<div class="contenedor-texto col-sm-5">
						<h3>ESTAMOS AQUÍ PARA ASESORARTE EN LA COMPRA DE UN SEGURO VEHICULAR</h3>
					</div>
					<div class="col-sm-1">
						<img src="{{asset('asset/img/double-arrow-right.png')}}" alt="">
					</div>
					<div class="boton-cotizar col-sm-3 hidden-xs hidden-sm">
						<button type="button" class="btn btn-cotizar" id="boton-cotizar-stan"data-toggle="modal" data-target=".cotizar-modal-lg"><img src="{{asset('asset/img/cotiza-stan_11.png')}}"></button>
						<button type="button" class="btn btn-cotizar btn-cotizarHover" data-toggle="modal" data-target=".cotizar-modal-lg"><img src="{{asset('asset/img/cotiza-hover_11.png')}}"></button>
					</div>
					<div class="boton-cotizar col-sm-3 hidden-md hidden-lg">
						<a href="#homederecha">
							<button type="button" class="btn btn-cotizar"><img src="{{asset('asset/img/cotiza-stan_11.png')}}"></button>
						</a>
					</div>		
				</div>
			</div>
		</div>
	</div>

	@include('principal.modulo.preguntas-frecuentes')


	<div id="boton-fijo" class="hidden-xs hidden-sm">
		<button type="button" class="boton-fijo escalar" data-toggle="modal" data-target=".cotizar-modal-lg"><img src="{{asset('asset/img/boton-fijo.png')}}" alt=""></button>
	</div>

	<div class="modal fade cotizar-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="container">
						<div class="row">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="form-modal-close"><span aria-hidden="true"><img src="{{asset('asset/img/close.png')}}" alt=""></span></button>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="homemodal">
						<div class="container">
							<div class="row">
								<div class="home-izquierda col-xs-12 col-md-7">
									<div class="home-izquierda-content col-xs-12 col-md-10 col-md-offset-1 text-center">
										<h1>COTIZA TU SEGURO VEHICULAR </h1>
										<h5>ENCUENTRA LA MEJOR OPCIÓN EN SEGUNDOS</h5>
										<h3>Te asesoramos GRATIS</h3>
										<div class="poner-telefono col-md-12">
											<div class="input-telefono col-md-8 col-md-offset-2">
												<input type="text" class="form-control" placeholder="Escribe tu teléfono" id="nro_dos">
											</div>
										</div>
										<p>Un asesor se pondrá encontacto contigo</p>
										<div class="poner-telefono col-md-12">
											<div class="boton-llamar col-md-8 col-md-offset-2">
												<button type="button" class="btn-llamar" id="formCotizarButton_dos">¡Llámame ahora!</button>
											</div>
										</div>
										<p>Llamanos al <b>(01) 640-0778</b></p>
										<div class="poner-telefono col-md-12">
											<div class="boton-cotizar-online col-md-8 col-md-offset-2 hidden-lg hidden-md">
												<!-- <input type="button" class="" value="Cotizar Online" data-toggle="modal" data-target="#modalmenu"> -->
												<a href="" class="btn btn-encontrar green" href="#homederecha">Cotizar Online</a>
											</div>
										</div>
									</div>
								</div>
								<div class="home-derecha hidden-xs hidden-sm col-md-5" >


									@include('principal.modulo.form', array('tipo' => 'modal'))



								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="boxes2" style="display: none;">
		<div id="dialog2" class="window2">
			<div class="call4">
				<img src="{{asset('static/images/call.png')}}" alt="">
			</div>
			<div class="call5">
				<img src="{{asset('static/images/logo-popa.png')}}" alt=""><br><br>
				<p>En un momento te estaremos llamando, muchas gracias!</p>
				<p class="call6">¡Gracias!</p>
			</div>
		</div>
		<div id="mask2" style="width: 1519px; height: 3031px; display: block; opacity: 0.8;"></div>
	</div>

	<div class="modal fade menu-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-menu">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="container">
						<div class="row">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="{{asset('asset/img/close.png')}}" alt="" id="close-menu-modal"></span></button>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="menumodal">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<ul>
										<li><a href="#fold">Inicio</a></li>
										<li><a href="#como-funciona">Como Funciona</a></li>
										<li><a data-toggle="modal" data-target=".cotizar-modal-lg" style="cursor:pointer">Cotiza Ahora</a></li>
										<li><a href="#beneficios-experiencia">Beneficios</a></li>
										<li><a href="#faq">Preguntas</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade menuresponsive-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="container">
						<div class="row">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="close-menu-modal-responsive"><span aria-hidden="true"><img src="{{asset('asset/img/close.png')}}" alt=""></span></button>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="menumodal">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<ul>
										<li><a href="#fold">Inicio</a></li>
										<li><a href="#como-funciona">Como Funciona</a></li>
										<li><a class="cotiza-ahora-mobile" href="#homederecha" >Cotiza Ahora</a></li>
										<li><a class="label-beneficios" href="#beneficios-experiencia">Beneficios</a></li>
										<li><a href="#faq">Preguntas</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="https://code.jquery.com/jquery-3.1.0.min.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"  ></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>


	<script type="text/javascript">

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.navbar-fixed-top').addClass('navbar-activo-fixed');
				$('.navbar-fixed-top').removeClass('navbar-initial');
			} else {
				$('.navbar-fixed-top').removeClass('navbar-activo-fixed');
				$('.navbar-fixed-top').addClass('navbar-initial');
			}
		});

		var a = $(".navbar-fixed-top").offset().top;

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.navbar-fixed-top').css({"background":"white"});
			} else {
				$('.navbar-fixed-top').css({"background":"transparent"});
			}
		});

		var a = $(".navbar-fixed-top").offset().top;


		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.logo-scroll').css({"opacity":"1"});
			} else {
				$('.logo-scroll').css({"opacity":"0"});
			}
		});

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.menu-icon').css({"opacity":"1"});
			} else {
				$('.menu-icon').css({"opacity":"0"});
			}
		});

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.llamanos-al').css({"color":"#3b2c33"});
			} else {
				$('.llamanos-al').css({"color":"white"});
			}
		});


		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.num-telf').css({"color":"#3b2c33"});
			} else {
				$('.num-telf').css({"color":"white"});
			}
		});


		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.logo-responsive').css({"opacity":"1"});
			} else {
				$('.logo-responsive').css({"opacity":"0"});
			}
		});

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.call-responsive').css({"opacity":"1"});
			} else {
				$('.call-responsive').css({"opacity":"0"});
			}
		});

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.menu-responsive').css({"opacity":"1"});
			} else {
				$('.menu-responsive').css({"opacity":"0"});
			}
		});

		$(document).scroll(function(){
			if($(this).scrollTop() > 10)
			{   
				$('.cotiza-responsive').css({"opacity":"1"});
			} else {
				$('.cotiza-responsive').css({"opacity":"0"});
			}
		});

		$(document).scroll(function(){
			if($(this).scrollTop() > 400)
			{   
				$('.boton-fijo').css({"display":"block"});
			} else {
				$('.boton-fijo').css({"display":"none"});
			}
		});

		$('#modal-menu a').on('click', function(){
			$('#close-menu-modal').click();
			$('#close-menu-modal-responsive').click();
			$('#form-modal-close').click();
		});

	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
			$('#mask2').css({'width':maskWidth,'height':maskHeight});
			$('#mask2').fadeIn(1000);	
			$('#mask2').fadeTo("slow",0.8);	
			var winH = $(window).height();
			var winW = $(window).width();
			$('#dialog2').css('top',  winH/2-$('#dialog2').height()/2);
			$('#dialog2').css('left', winW/2-$('#dialog2').width()/2);
			$('#dialog2').fadeIn(2000);
		});
		$('#mask2').on('click',function(){
			$('#boxes2').hide();
		});
		$('.menumodal a').on('click',function(){
			$('#close-menu-modal').click();
			$('#close-menu-modal-responsive').click();
			$('#form-modal-close').click();
		});
		$('.menumodal a.cotiza-ahora-mobile').on('click', function(){
			$('#close-menu-modal').click();
			$('#close-menu-modal-responsive').click();
			$('#form-modal-close').click();
			var $anchor = $(this);
			$('html, body').stop().animate({scrollTop: $($anchor.attr('href')).offset().top - 65});
		});
		$('.btn-encontrar.green').on('click', function(){
			$('.menumodal a.cotiza-ahora-mobile').click();
		});
		$('.boton-cotizar.hidden-md.hidden-lg').on('click', function(){
			$('.menumodal a.cotiza-ahora-mobile').click();
		});
		$('.menumodal a.label-beneficios').on('click', function(){
			var $anchor = $(this);
			$('html, body').stop().animate({scrollTop: $($anchor.attr('href')).offset().top - 40});
		});
	</script>

	<script type="text/javascript">


		$(document).ready(function() {


			$('#formCotizarButton').click(function(e){
				e.preventDefault();
				e.stopPropagation(); 
				var phone = $("#nro").val();
				console.log(phone);
				window.phone = phone;  
				console.log(phone);
				if(phone.length>6 && phone.length<=9 ){
					$.ajax({
						url :'http://dilootu.com/c2capi/',
						type:'POST',
						data:{
							username: 'apiaggiov@gmail.com',
							password: 'Jto86VG4F7',
							numero1:'923607626',
							numero2:phone
						},
						complete:function(xhr,status){
							console.log(status);
							$('#boxes2').fadeIn( "slow" );
						}
					});
				}else{
					alert("INGRESE UN NÚMERO FIJO O CELULAR VÁLIDO");
				}
			});

			$('#formCotizarButton_dos').click(function(e){
				e.preventDefault();
				e.stopPropagation(); 
				var phone = $("#nro_dos").val();
				console.log(phone);
				window.phone = phone;  
				console.log(phone);
				if(phone.length>6 && phone.length<=9 ){
					$.ajax({
						url :'http://dilootu.com/c2capi/',
						type:'POST',
						data:{
							username: 'apiaggiov@gmail.com',
							password: 'Jto86VG4F7',
							numero1:'923607626',
							numero2:phone
						},
						complete:function(xhr,status){
							console.log(status);
							$('#boxes2').fadeIn( "slow" );
						}
					});
				}else{
					alert("INGRESE UN NÚMERO FIJO O CELULAR VÁLIDO");
				}
			});


		});


	</script>

	<script type="text/javascript">

		// $("#form-derecha .marca").on('change', function traerModelo  () {
		// 	$("#form-derecha .marca option:selected").each(function () {
		// 		id_marca = $(this).val();
		// 		ruta = "{{ route('modelos.lista') }}";
		// 		console.log(id_marca);
		// 		$.ajax({
		// 			type: 'POST',
		// 			headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
		// 			url: ruta,
		// 			data: { id_marca: id_marca },
		// 			beforeSend: function() {
		// 				$("#form-derecha .modelo").empty();
		// 			},
		// 			complete: function(data) {
  //                   //console.log('complete');
  //               },
  //               success: function (data) {
  //                   // console.log(data);
  //                   $("#form-derecha .modelo").html(data);
  //               },
  //               error: function(errors) {
  //                         //
  //                     }
  //                 });

		// 	})
		// });
		// $("#form-derecha .modelo").on('change', function traerTipo  () {
		// 	$("#form-derecha .modelo option:selected").each(function () {
		// 		id_modelo = $(this).val();
		// 		ruta = "{{ route('tipo.lista') }}";
		// 		console.log(id_modelo);
		// 		$.ajax({
		// 			type: 'POST',
		// 			headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
		// 			url: ruta,
		// 			data: { id_modelo: id_modelo },
		// 			beforeSend: function() {
		// 				$("#form-derecha .tipo").empty();
		// 			},
		// 			complete: function(data) {
  //                   //console.log('complete');
  //               },
  //               success: function (data) {
  //               	console.log(data);
  //               	$("#form-derecha .tipo").html(data);
  //               },
  //               error: function(errors) {
  //                         //
  //                     }
  //                 });

		// 	})
		// });
		// $("#form-derecha .tipo").on('change', function traerTipo  () {
		// 	$("#form-derecha .tipo option:selected").each(function () {
		// 		id_tipo = $(this).val();
		// 		ruta = "{{ route('uso.lista') }}";
		// 		console.log(id_tipo);
		// 		$.ajax({
		// 			type: 'POST',
		// 			headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
		// 			url: ruta,
		// 			data: { id_tipo: id_tipo },
		// 			beforeSend: function() {
		// 				$("#form-derecha .usoSelect").empty();
		// 			},
		// 			complete: function(data) {
  //                   //console.log('complete');
  //               },
  //               success: function (data) {
  //               	console.log(data);
  //               	$("#form-derecha .usoSelect").html(data);
  //               },
  //               error: function(errors) {
  //                         //
  //                     }
  //                 });

		// 	})
		// });
		// $("#form-derecha .anio").on('change', function traerAnio  () {
		// 	$("#form-derecha .anio option:selected").each(function () {
		// 		id_anio = $(this).val();
		// 		console.log(id_anio);
		// 		if ( id_anio == 28 || id_anio == 29 ) {
		// 			$('#form-derecha .tipo-auto option:eq(0)').prop('selected', true);
		// 		}
		// 		else{
		// 			$('#form-derecha .tipo-auto option:eq(1)').prop('selected', true);
		// 		}
		// 	})
		// });
		$(document).on('click','.datos-vehiculo', function(e){
			e.preventDefault();
			$('.datos-vehiculo').addClass('activo');
			$('.datos-personales').removeClass('activo');
			$('.paso-1').addClass('activo');
			$('.paso-2').removeClass('activo');
		});
		$(document).on('click','.datos-personales', function(e){
			e.preventDefault();
			$('.datos-vehiculo').removeClass('activo');
			$('.datos-personales').addClass('activo');
			$('.paso-1').removeClass('activo');
			$('.paso-2').addClass('activo');
		});
		$(document).ready(function(){
			alto = $('.paso-1').height();
			$('.paso-2').height(alto);
		});
		$(document).on('click','#form-derecha .ir-paso-2', function(e){
			e.preventDefault();
			var estado = true;
			if ( $("#form-derecha .marca option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .modelo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .tipo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .anio option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .nuevo_usado option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .usoSelect option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .valor_actual").val() == '' ) {
				estado = false;
			}

			if ( estado == false ) {
				alert('Por favor, completa todos los datos!')
				return false;
			}

			$('.datos-personales').click();
		});
		$(document).on('click','#form-derecha .btn-terminar-paso-1', function(e){
			e.preventDefault();
			var estado = true;
			if ( $("#form-derecha .marca option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .modelo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .tipo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .anio option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .nuevo_usado option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .usoSelect option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-derecha .valor_actual").val() == '' ) {
				estado = false;
			}

			if ( estado == false ) {
				alert('Por favor, completa todos los datos!')
				return false;
			}

			$('#form-derecha .datos-personales').click();
		});
		$(document).on('click','#form-derecha .enviar-info', function(e){
			e.preventDefault();
			var marca 			= $("#form-derecha .marca option:selected").val();
			var modelo 			= $("#form-derecha .modelo option:selected").val();
			var tipo 			= $("#form-derecha .tipo option:selected").val();
			var anio 			= $("#form-derecha .anio option:selected").val();
			var nuevo_usado 	= $("#form-derecha .nuevo_usado option:selected").val();
			var uso 			= $("#form-derecha .usoSelect option:selected").val();
			var valor_actual 	= $("#form-derecha .valor_actual").val();
			var ubicacion 		= $("#form-derecha input[name=ubicacion_derecha]:checked").val();
			var nombre 			= $("#form-derecha .nombre").val();
			var email 			= $("#form-derecha .email").val();
			var celular 		= $("#form-derecha .celular").val();

			if (marca == '' || modelo == '' || tipo == '' || anio == '' || nuevo_usado == '' || uso == '' || valor_actual == '' || ubicacion == '' || nombre == '' || email == '' || celular == '') {
				alert('Ingresa todos los datos');
				return false;
			}
			console.log('marca: '+marca+' modelo: '+modelo+' tipo: '+tipo+' anio: '+anio+' nuevo_usado: '+nuevo_usado+' uso: '+uso+' valor_actual: '+valor_actual+' ubicacion: '+ubicacion+' nombre: '+nombre+' email: '+email+' celular: '+celular)
			ruta = "{{ route('form.store') }}";
			$.ajax({
				type: 'POST',
				headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
				url: ruta,
				data: { marca:marca, modelo:modelo,tipo:tipo,anio:anio,nuevo_usado:nuevo_usado,uso:uso,valor_actual:valor_actual,ubicacion:ubicacion,nombre:nombre,email:email,celular:celular },
					beforeSend: function() {
						// $("#form-derecha .nombre").val('');
						// $("#form-derecha .email").val('');
						// $("#form-derecha .celular").val('');
					},
					complete: function(data) {
						// $("#form-derecha .nombre").val('');
						// $("#form-derecha .email").val('');
						// $("#form-derecha .celular").val('');
					},
					success: function (data) {
	                // console.log(data);
	                $('#boxes2').fadeIn( "slow" );
	                $('#close-menu-modal').click();
	                $('#close-menu-modal-responsive').click();
	                $('#form-modal-close').click();
	            },
	            error: function(errors) {
	                //
	            }
	        });


			var marca 			= $("#form-derecha .marca option:selected").html();
			var modelo 			= $("#form-derecha .modelo option:selected").html();
			var tipo 			= $("#form-derecha .tipo option:selected").html();
			var anio 			= $("#form-derecha .anio option:selected").html();
			var nuevo_usado 	= $("#form-derecha .nuevo_usado option:selected").html();
			var uso 			= $("#form-derecha .usoSelect option:selected").html();
			var valor_actual 	= $("#form-derecha .valor_actual").val();
			var ubicacion 		= $("#form-derecha input[name=ubicacion_derecha]:checked").val();
			var nombre 			= $("#form-derecha .nombre").val();
			var email 			= $("#form-derecha .email").val();
			var celular 		= $("#form-derecha .celular").val();

			console.log('marca: '+marca+' - modelo: '+modelo+' - tipo: '+tipo+' - anio: '+anio+' - nuevo_usado: '+nuevo_usado+' - uso: '+uso+' - valor_actual: '+valor_actual+' - ubicacion: '+ubicacion+' - nombre: '+nombre+' - email: '+email+' - celular: '+celular);

	        <?php $ruta_dos = route('principal.home').'/formularioAsegura.php'; ?>
	        ruta_dos = "{{$ruta_dos}}";
	        $.ajax({
				type: 'POST',
				headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
				url: ruta_dos,
				data: { marca:marca, modelo:modelo,tipo:tipo,anio:anio,nuevo_usado:nuevo_usado,uso:uso,valor_actual:valor_actual,ubicacion:ubicacion,nombre:nombre,email:email,celular:celular },
					beforeSend: function() {
						$("#form-derecha .nombre").val('');
						$("#form-derecha .email").val('');
						$("#form-derecha .celular").val('');
					},
					complete: function(data) {
						$("#form-derecha .nombre").val('');
						$("#form-derecha .email").val('');
						$("#form-derecha .celular").val('');
					},
					success: function (data) {
	                // console.log(data);
	                $('#boxes2').fadeIn( "slow" );
	                $('#close-menu-modal').click();
	                $('#close-menu-modal-responsive').click();
	                $('#form-modal-close').click();
	            },
	            error: function(errors) {
	                //
	            }
	        });
		});

		setTimeout(function(){
			$("#form-derecha #lima-derecha").attr('checked','checked');
		},10);


	</script>



	<script type="text/javascript">

		// $("#form-modal .marca").on('change', function traerModelo  () {
		// 	$("#form-modal .marca option:selected").each(function () {
		// 		id_marca = $(this).val();
		// 		ruta = "{{ route('modelos.lista') }}";
		// 		console.log(id_marca);
		// 		$.ajax({
		// 			type: 'POST',
		// 			headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
		// 			url: ruta,
		// 			data: { id_marca: id_marca },
		// 			beforeSend: function() {
		// 				$("#form-modal .modelo").empty();
		// 			},
		// 			complete: function(data) {
  //                   //console.log('complete');
  //               },
  //               success: function (data) {
  //                   // console.log(data);
  //                   $("#form-modal .modelo").html(data);
  //               },
  //               error: function(errors) {
  //                   //
  //               }
  //           });
		// 	})
		// });
		// $("#form-modal .modelo").on('change', function traerTipo  () {
		// 	$("#form-modal .modelo option:selected").each(function () {
		// 		id_modelo = $(this).val();
		// 		ruta = "{{ route('tipo.lista') }}";
		// 		console.log(id_modelo);
		// 		$.ajax({
		// 			type: 'POST',
		// 			headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
		// 			url: ruta,
		// 			data: { id_modelo: id_modelo },
		// 			beforeSend: function() {
		// 				$("#form-modal .tipo").empty();
		// 			},
		// 			complete: function(data) {
  //                   //console.log('complete');
  //               },
  //               success: function (data) {
  //               	console.log(data);
  //               	$("#form-modal .tipo").html(data);
  //               },
  //               error: function(errors) {
  //                   //
  //               }
  //           });
		// 	})
		// });
		// $("#form-modal .tipo").on('change', function traerTipo  () {
		// 	$("#form-modal .tipo option:selected").each(function () {
		// 		id_tipo = $(this).val();
		// 		ruta = "{{ route('uso.lista') }}";
		// 		console.log(id_tipo);
		// 		$.ajax({
		// 			type: 'POST',
		// 			headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
		// 			url: ruta,
		// 			data: { id_tipo: id_tipo },
		// 			beforeSend: function() {
		// 				$("#form-modal .usoSelect").empty();
		// 			},
		// 			complete: function(data) {
  //                   //console.log('complete');
  //               },
  //               success: function (data) {
  //               	console.log(data);
  //               	$("#form-modal .usoSelect").html(data);
  //               },
  //               error: function(errors) {
  //                   //
  //               }
  //           });
		// 	})
		// });
		// $("#form-modal .anio").on('change', function traerAnio  () {
		// 	$("#form-modal .anio option:selected").each(function () {
		// 		id_anio = $(this).val();
		// 		console.log(id_anio);
		// 		if ( id_anio == 28 || id_anio == 29 ) {
		// 			$('#form-modal .tipo-auto option:eq(0)').prop('selected', true);
		// 		}
		// 		else{
		// 			$('#form-modal .tipo-auto option:eq(1)').prop('selected', true);
		// 		}
		// 	})
		// });
		$(document).on('click','.datos-vehiculo', function(e){
			e.preventDefault();
			$('.datos-vehiculo').addClass('activo');
			$('.datos-personales').removeClass('activo');
			$('.paso-1').addClass('activo');
			$('.paso-2').removeClass('activo');
		});
		$(document).on('click','.datos-personales', function(e){
			e.preventDefault();
			$('.datos-vehiculo').removeClass('activo');
			$('.datos-personales').addClass('activo');
			$('.paso-1').removeClass('activo');
			$('.paso-2').addClass('activo');
		});
		$(document).ready(function(){
			alto = $('.paso-1').height();
			$('.paso-2').height(alto);
		});
		$(document).on('click','#form-modal .ir-paso-2', function(e){
			e.preventDefault();
			var estado = true;
			if ( $("#form-modal .marca option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .modelo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .tipo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .anio option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .nuevo_usado option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .usoSelect option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .valor_actual").val() == '' ) {
				estado = false;
			}
			if ( estado == false ) {
				alert('Por favor, completa todos los datos!')
				return false;
			}

			$('#form-modal .datos-personales').click();
		});

		$(document).on('click','#form-modal .btn-terminar-paso-1', function(e){
			e.preventDefault();
			var estado = true;
			if ( $("#form-modal .marca option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .modelo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .tipo option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .anio option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .nuevo_usado option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .usoSelect option:selected").val() == '' ) {
				estado = false;
			}
			if ( $("#form-modal .valor_actual").val() == '' ) {
				estado = false;
			}

			if ( estado == false ) {
				alert('Por favor, completa todos los datos!')
				return false;
			}

			$('.datos-personales').click();
		});
		$(document).on('click','#form-modal .enviar-info', function(e){
			e.preventDefault();
			var marca 			= $("#form-modal .marca option:selected").val();
			var modelo 			= $("#form-modal .modelo option:selected").val();
			var tipo 			= $("#form-modal .tipo option:selected").val();
			var anio 			= $("#form-modal .anio option:selected").val();
			var nuevo_usado 	= $("#form-modal .nuevo_usado option:selected").val();
			var uso 			= $("#form-modal .usoSelect option:selected").val();
			var valor_actual 	= $("#form-modal .valor_actual").val();
			var ubicacion 		= $("#form-modal input[name=ubicacion_modal]:checked").val();
			var nombre 			= $("#form-modal .nombre").val();
			var email 			= $("#form-modal .email").val();
			var celular 		= $("#form-modal .celular").val();
			if (marca == '' || modelo == '' || tipo == '' || anio == '' || nuevo_usado == '' || uso == '' || valor_actual == '' || ubicacion == '' || nombre == '' || email == '' || celular == '') {
				alert('Ingresa todos los datos');
				return false;
			}
			console.log('marca: '+marca+' modelo: '+modelo+' tipo: '+tipo+' anio: '+anio+' nuevo_usado: '+nuevo_usado+' uso: '+uso+' valor_actual: '+valor_actual+' ubicacion: '+ubicacion+' nombre: '+nombre+' email: '+email+' celular: '+celular)
			ruta = "{{ route('form.store') }}";
			$.ajax({
					type: 'POST',
					headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
					url: ruta,
					data: { marca:marca, modelo:modelo,tipo:tipo,anio:anio,nuevo_usado:nuevo_usado,uso:uso,valor_actual:valor_actual,ubicacion:ubicacion,nombre:nombre,email:email,celular:celular },
					beforeSend: function() {
						// $("#form-modal .nombre").val('');
						// $("#form-modal .email").val('');
						// $("#form-modal .celular").val('');
					},
					complete: function(data) {
						// $("#form-modal .nombre").val('');
						// $("#form-modal .email").val('');
						// $("#form-modal .celular").val('');
					},
					success: function (data) {
	                // console.log(data);
	                $('#boxes2').fadeIn( "slow" );
	                $('#close-menu-modal').click();
	                $('#close-menu-modal-responsive').click();
	                $('#form-modal-close').click();
	            },
	            error: function(errors) {
	                //
	            }
	        });

	        var marca 			= $("#form-derecha .marca option:selected").html();
			var modelo 			= $("#form-derecha .modelo option:selected").html();
			var tipo 			= $("#form-derecha .tipo option:selected").html();
			var anio 			= $("#form-derecha .anio option:selected").html();
			var nuevo_usado 	= $("#form-derecha .nuevo_usado option:selected").html();
			var uso 			= $("#form-derecha .usoSelect option:selected").html();
			var valor_actual 	= $("#form-derecha .valor_actual").val();
			var ubicacion 		= $("#form-derecha input[name=ubicacion_derecha]:checked").val();
			var nombre 			= $("#form-derecha .nombre").val();
			var email 			= $("#form-derecha .email").val();
			var celular 		= $("#form-derecha .celular").val();

			console.log('marca: '+marca+' - modelo: '+modelo+' - tipo: '+tipo+' - anio: '+anio+' - nuevo_usado: '+nuevo_usado+' - uso: '+uso+' - valor_actual: '+valor_actual+' - ubicacion: '+ubicacion+' - nombre: '+nombre+' - email: '+email+' - celular: '+celular);

			
	        <?php $ruta_dos = route('principal.home').'/formularioAsegura.php'; ?>
	        ruta_dos = "{{$ruta_dos}}";
	        $.ajax({
				type: 'POST',
				headers: {'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')},
				url: ruta_dos,
				data: { marca:marca, modelo:modelo,tipo:tipo,anio:anio,nuevo_usado:nuevo_usado,uso:uso,valor_actual:valor_actual,ubicacion:ubicacion,nombre:nombre,email:email,celular:celular },
					beforeSend: function() {
						$("#form-modal .nombre").val('');
						$("#form-modal .email").val('');
						$("#form-modal .celular").val('');
					},
					complete: function(data) {
						$("#form-modal .nombre").val('');
						$("#form-modal .email").val('');
						$("#form-modal .celular").val('');
					},
					success: function (data) {
	                // console.log(data);
	                $('#boxes2').fadeIn( "slow" );
	                $('#close-menu-modal').click();
	                $('#close-menu-modal-responsive').click();
	                $('#form-modal-close').click();
	            },
	            error: function(errors) {
	                //
	            }
	        });
		});

		setTimeout(function(){
			$("#form-modal #lima-modal").attr('checked','checked');
		},10);


	</script>
	@yield('js')
	<script type="text/javascript">
		window.$zopim||(function(d,s){
			var z=$zopim=function(c){
				z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.push(o)};z=[];z.set=[];$.async=!0;$.setAttribute('charset','utf-8');$.src='https://v2.zopim.com/?4dk8Xz2bygCDU6boJZamYYm5pladqMHY';z.t=+new Date;$.type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
			</script>
		</body>
		</html>