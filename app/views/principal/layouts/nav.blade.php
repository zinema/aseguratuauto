<div id="header" class="navbar-fixed-top navbar-initial">
	<div class="container">
		<div class="row">
			<div class="telefono col-sm-12 hidden-xs hidden-sm">
				<div class="logo-scroll col-sm-3">
					<a href="{{ route('principal.home') }}">
						<img src="{{asset('asset/img/logo-responsive.png')}}" alt="">
					</a>
				</div>
				<div class="col-sm-4">
				</div>
				<div class="telefono col-sm-4">
					<p class="llamanos-al">Llamanos al</p>
					<h3 class="num-telf">(01) 640-0778</h3>
				</div>
				<div class="menu-icon col-sm-1">
					<button type="button" class="escalar" data-toggle="modal" data-target=".menu-modal-lg"><img src="{{asset('asset/img/menu.png')}}" alt=""></button>
				</div>						
			</div>
			<div class="col-xs-12 telefono hidden-md hidden-lg">
				<p class="llamanos-al">Llamanos al</p>
				<h3 class="num-telf">(01) 640-0778</h3>
			</div>
		</div>
	</div>
</div>
<div id="header-responsive" class="navbar-fixed-top  hidden-md hidden-lg hidden-">
	<div class="container">
		<div class="row">
			<div class="telefono col-xs-12 ">
				<div class="col-xs-4 ">
					<img src="{{asset('asset/img/logo-responsive.png')}}" class="logo-responsive">
				</div>
				<div class="col-xs-5">
					<a href="#homederecha"><img src="{{asset('asset/img/cotiza-responsive.png')}}" alt="" class="cotiza-responsive"></a>
				</div>
				<div class="col-xs-2 centrar">
					<button type="button" class="call-responsive btn-modal" data-toggle="modal" data-target=".cotizar-modal-lg"><img src="{{asset('asset/img/call-responsive.png')}}" alt=""></button>
				</div>
				<div class="col-xs-2 centrar">
					<button type="button" class="menu-responsive btn-modal" data-toggle="modal" data-target=".menuresponsive-modal-lg"><img src="{{asset('asset/img/menu-responsive.png')}}" alt=""></button>
				</div>
			</div>
		</div>
	</div>
</div>