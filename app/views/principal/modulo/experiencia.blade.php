<div id="beneficios-experiencia">
	<div class="container">
		<div class="row">
			<div class="beneficios-titulo col-md-12">
				<h3>Más de 30 años de experiencia<br>en el mercado de seguros</h3>
			</div>
			<div class="beneficios col-md-12">
				<div class="beneficios-izquierda col-xs-12 col-md-3 text-right">
					<h4 class="escalar"><b>Beneficios</b></h4>
					<h4>para nuestros afiliados</h4>
					<div class="boton-descargar">
						<a href="http://hermesplus.hermes.pe/beneficios.php#loaded" target="_blank">
							<button type="button" class="btn-descargar"><img src="{{asset('asset/img/descarga-stan_07.png')}}"></button>
							<button type="button" class="btn-descargarHover"><img src="{{asset('asset/img/descargar-hover_07.png')}}"></button>
						</a>
					</div>
					<p>Nuestra cartilla de beneficios</p>
				</div>
				<div class="arrow-left col-md-1 hidden-xs hidden-sm">
					<img src="{{asset('asset/img/arrow-point-to-right.png')}}" alt="">
				</div>
				<div class="arrow-down col-md-4 hidden-md hidden-lg hidden-xl">
					<img src="{{asset('asset/img/arrow-down.png')}}" alt="">
				</div>
				<div class="beneficios-derecha col-xs-12 col-md-8">
					<div class="beneficios-icono col-xs-4 col-sm-4 text-center">
						<img src="{{asset('asset/img/24-hours-delivery.png')}}" alt="">
						<p>Asistencia en<br>Siniestros las 24h</p>
					</div>
					<div class="beneficios-icono col-xs-4 col-sm-4 text-center">
						<img src="{{asset('asset/img/hand-holding-up-a-wrench.png')}}" alt="">
						<p>Descuentos en<br>Talleres y Autopartes</p>
					</div>
					<div class="beneficios-icono col-xs-4 col-sm-4 text-center">
						<img src="{{asset('asset/img/heart-line.png')}}" alt="">
						<p class="consultas">Consultas<br>Médicas</p>
					</div>
				</div>
			</div>
			<div class="beneficio-texto col-md-12">
				<h3>!Muchos descuentos mas te están esperando!</h3>
				<p class="">Al afiliarte a nuestra red de clientes recibirás una cartilla de descuento(Hermes Plus) donde le ofreceremos a usted y a sus familiares cercanos (padres, hermanos y abuelos), acceso a una gran gama de beneficios, de manera completamente gratuita</p>
			</div>

		</div>
	</div>
</div>