<div id="como-funciona" style="background-image: url({{asset('asset/img/como_funciona.png')}});background-size: cover;background-position: center center;">
	<div class="container">
		<div class="row text-center">
			<h3>¿CÓMO FUNCIONA?</h3>
			<div class="col-xs-12 hidden-lg hidden-md hidden-sm">
				<div class="responsive col-xs-12 col-sm-4">
					<img src="{{asset('asset/img/fun-1r.png')}}" alt="">
					<p>Ingresa los datos de tu vehiculo y de contacto; si necesitas asistencia escribe tu teléfono y dale click en "Llámame".</p>
				</div>
				<div class="responsive col-xs-12 col-sm-4">
					<img src="{{asset('asset/img/fun-2r.png')}}" alt="">
					<p>Un asesor se comunicará contigo y ayudará a encontrar el seguro que más te conviene.</p>
				</div>
				<div class="responsive col-xs-12 col-sm-4">
					<img src="{{asset('asset/img/fun-3r.png')}}" alt="">
					<p>una vez que escojas el seguro de tu preferencia, te ayudamos a elegir la mejor forma de pago (*los pagos se realizan directamente a la cia. aseguradora que elijas).</p>
				</div>
			</div>
			<div class="col-md-12 hidden-xs">
				<div class="col-sm-4">
					<img src="{{asset('asset/img/fun-1.png')}}" alt="">
					<p>Ingresa los datos de tu vehiculo y de contacto; si necesitas asistencia escribe tu teléfono y dale click en "Llámame".</p>
				</div>
				<div class="col-sm-4">
					<img src="{{asset('asset/img/fun-2.png')}}" alt="">
					<p>Un asesor se comunicará contigo y ayudará a encontrar el seguro que más te conviene.</p>
				</div>
				<div class="col-sm-4">
					<img src="{{asset('asset/img/fun-3.png')}}" alt="">
					<p>una vez que escojas el seguro de tu preferencia, te ayudamos a elegir la mejor forma de pago (*los pagos se realizan directamente a la cia. aseguradora que elijas).</p>
				</div>
			</div>
		</div>
	</div>
</div>