<div class="brands-seguro col-md-12 text-left">
	<a href="{{route('principal.home')}}/seguro/lapositiva" >
		<img src="{{asset('asset/img/logos/positiva_color.png')}}" alt="" class="on">
	</a>
	<a href="{{route('principal.home')}}/seguro/rimac" >
		<img src="{{asset('asset/img/logos/rimac_blanco.png')}}" alt="" class="logo-blanco">
		<img src="{{asset('asset/img/logos/rimac-color.png')}}" alt="" class="logo-color">
	</a>
	<a href="{{route('principal.home')}}/seguro/pacificoseguros" >
		<img src="{{asset('asset/img/logos/pacifico_blanco.png')}}" alt="" class="logo-blanco">
		<img src="{{asset('asset/img/logos/pacifico-color.png')}}" alt="" class="logo-color">
	</a>
	<a href="{{route('principal.home')}}/seguro/mapfre" >
		<img src="{{asset('asset/img/logos/mapfre-color.png')}}" alt="" class="on">
	</a>
	<a href="{{route('principal.home')}}/seguro/hdiseguros" >
		<img src="{{asset('asset/img/logos/hdi-logo.png')}}" alt="" class="on">
	</a>
</div>