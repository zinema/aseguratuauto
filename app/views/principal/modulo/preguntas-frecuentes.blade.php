<div id="faq">
	<div class="container">
		<div class="row">
			<div class="co-md-12">
				<div class="col-md-6">
					<h4>Preguntas Frecuentes</h4>
					<div id="MainMenu">
						<div class="list-group panel">

							@foreach($preguntas as $key => $pregunta)
							<a href="#collapse12{{$pregunta->id}}" class="pregunta list-group-item" data-toggle="collapse" data-parent="#MainMenu">{{ $pregunta->titulo }}<i class="fa fa-caret-down"></i></a>
							<div class="collapse" id="collapse12{{$pregunta->id}}">
								<p>
									{{ $pregunta->contenido }}
								</p>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>