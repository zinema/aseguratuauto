<div class="col-sm-12" id="form-{{$tipo}}" ng-app="myApp" ng-controller="CotizaUser">
	<h3>Cotiza al instante</h3>
	<p>llenando el formulario</p>
	<div class="tab">
		<!-- Nav tabs -->
		<ul class="datos-tab nav nav-tabs" role="tablist">
			<li role="presentation" class=" datos active"><a class="datos-vehiculo activo" href="#datos-vehiculo-{{$tipo}}" aria-controls="datos-vehiculo-{{$tipo}}" role="tab" data-toggle="tab">
				<span>1</span>
				Datos vehiculos
			</a></li>
			<li role="presentation" class=" datos" ><a class="datos-personales" href="#datos-personal-{{$tipo}}" aria-controls="datos-personal-{{$tipo}}" role="tab" data-toggle="tab">
				<span>2</span>
				Datos personales
			</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="datos-vehiculo-{{$tipo}}">
				<div class="contenedor-opciones col-xs-12">	
					<div class="col-xs-5">
					

						<select ng-change= "obtenermodelo(model)" ng-options="item as item.name_marca for item in marca track by item.id_marca" type="text" class="opciones form-control col-xs-12 marca" ng-model="model.marca" required ="true"><option value="" selected="selected">Marca</option></select>

                        <select ng-change= "obteneruso(model)" ng-options="item as item.id_tipo__clase for item in claseModelo track by item.id" type="text" class="opciones form-control col-xs-12 tipo" ng-model="model.claseModelo" required ="true"  ><option value="" selected="selected" >Tipo</option></select>

		            	<select style="display: inline-block;margin-top: 10px; padding-left: 5px !important;" ng-change='evaluaanio(model.anio)' ng-options="item as item.anio_antig for item in anio track by item.id_anio" type="text" class="form-control" ng-model="model.anio" required ="true" ><option value="" selected="selected" >Anio</option></select>
                        </select>
					
					</div>

					<div class="col-xs-5">
						
						<select  ng-change= "obtenerclase(model)" ng-options="item as item.id_modelo__name_model for item in modelo track by item.id_modelo" type="text" class="opciones form-control col-xs-12 modelo" ng-model="model.modelo" required ="true"><option value="" selected="selected">Modelo</option></select>

						<select  style="display: inline-block;margin-top: 10px; padding-left: 5px !important;" ng-options="item.uso as item.uso__uso for item in uso" type="text" class=" form-control col-xs-12 tipo-auto nuevo_usado" ng-model="model.uso" required =""><option value="" selected="selected" >Uso</option></select>

						<div class="form-group" style="display: inline-block;margin-top: 20px; padding-left: 5px !important;">
							<input type="number" placeholder='Precio $USD' ng-model="precio" required ="true" class="form-control valor_actual" id="valor_actual" >
						</div>

						
						<!--  <div class="col-xs-12 region" style="margin-top: -5px;margin-left: 5px;" id="ubicacion">
							<p style="margin-bottom: 2px !important;">Ubicación</p>
							<div class="row">
								<div class="col-xs-6">
									<label class="radio-inline active"><input type="radio" name="ubicacion_{{$tipo}}" id="lima-{{$tipo}}" value="Lima">Lima</label>
								</div>
								<div class="col-xs-6">
									<label class="radio-inline"><input type="radio" name="ubicacion_{{$tipo}}" id="provincia-{{$tipo}}" value="Provincia">Provincia</label>		
								</div>
							</div>
						</div>  -->

					</div>
				</div>
				<div class="col-xs-12 hidden-xs boton-terminar">
					<button type="submit" class="btn-terminar ir-paso-2" id="ir-paso-2">Siguiente</button>
				</div>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg hidden-xl boton-terminar">
					<button type="submit" class="btn-terminar btn-terminar-paso-1" id="btn-terminar-paso-1">Siguiente</button>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="datos-personal-{{$tipo}}">
				<div class="col-xs-12">
					<form>
						<div class="form-group">
						<input type="text" ng-model='model.name' name="nombre" class="form-control nombre" id="nombre" placeholder="Nombre Completo">
							
						</div>
						<div class="form-group">
							<input type="email" name="email" ng-model="model.email"  class="form-control email" id="email" placeholder="Email">
						</div>
						<div class="form-group">
							<input type="text" name="celular" ng-model='model.cel' class="form-control celular" id="celular" placeholder="Celular">
						</div>
					
					</form>
					<div class="boton-encontrar col-sm-12">
						<input ng-click="saveContact(model,precio,check.ubicL,ubicP)"  type="button" class="btn btn-encontrar " value="Encuentra tu seguro vehicular" >
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>