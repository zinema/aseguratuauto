<?php $marcas = Marca::where('status',1)->get(); ?>
<div id="marcas-autos">
		<div class="container">	
			<div class="row">
				<div class="col-md-12">
					<!-- <p class="text-center">Te brindamos el seguro mas conveniente para tu automóvil</p> -->
					<div class="brands col-md-12 hidden-xs text-center">
						@foreach($marcas as $marca)
						<a href="{{route('principal.home')}}/marca/{{$marca->seo}}" >
							<img src="{{asset('uploads/'.$marca->imagen_marca)}}" alt="">
						</a>
						@endforeach
					</div>
					<div class="brands col-md-12 hidden-lg hidden-md hidden-sm text-center">
						<?php $k = 1 ?>
						<div class="brands-arriba col-xs-12">
							<div class="row">
								@foreach($marcas as $marca)
								<a href="{{route('principal.home')}}/marca/{{$marca->seo}}"  class="col-sm-3 col-xs-4">
									<img src="{{asset('uploads/'.$marca->imagen_marca)}}" alt="" class="brand-1 ">
								</a>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>