@extends('principal.layouts.master')

@section('content')

<div id="fold">
	@include('principal.layouts.nav')
	<div class="home">
		<div class="container">
			<div class="row">
				<div class="home-izquierda col-xs-12 col-md-7">
					<div class="logo col-xs-12 col-md-12">
						<div class="contenedor-logo col-xs-12 col-md-5">	
							<img src="{{asset('asset/img/logo-asegura.png')}}" alt="">
						</div>
						<div class="respaldo col-xs-12 col-md-7">
							<p>Con el respaldo de</p>
							<img src="{{asset('asset/img/logos/logo-hermes_blanco.png')}}">
						</div>
					</div>
					<div class="home-izquierda-content col-xs-12 col-md-12">
						<h1>COTIZA TU SEGURO VEHICULAR </h1>
						<h5>ENCUENTRA LA MEJOR OPCIÓN EN SEGUNDOS</h5>
						<h3>Te asesoramos GRATIS</h3>
						<div class="poner-telefono col-md-12">
							<div class="input-telefono col-md-7">
								<input type="text" class="form-control" placeholder="Escribe tu teléfono" id="nro" style="margin-top: 5px;margin-bottom: 5px;">
							</div>
							<div class="boton-llamar col-md-3">
								<button type="button" class="formCotizarButton" id="formCotizarButton"><img src="{{asset('asset/img/llamame-stan_03.png')}}"></button>
								<button type="button" class="formCotizarButtonHover" id="formCotizarButton"><img src="{{asset('asset/img/llamame-hover_03.png')}}"></button>
							</div>
						</div>
						<p class="hidden-xs hidden-sm">Un asesor se pondrá encontacto contigo</p>
						<p class="hidden-md hidden-lg hidden-xl">Estamos asociados con las <br> <b>mejores aseguradoras</b></p>
						<p class="hidden-xs hidden-sm">Estamos asociados con las mejores aseguradoras</p>
						@include('principal.modulo.seguros', array('tipo' => 'derecha'))
					</div>
				</div>
				<div class="home-derecha col-xs-12 col-md-5" id="homederecha">
					@include('principal.modulo.form', array('tipo' => 'derecha'))
				</div>
			</div>
		</div>
	</div>
</div>
@include('principal.modulo.marcas-autos')

@stop