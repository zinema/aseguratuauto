@extends('principal.layouts.master')

@section('metas')
<title>Aseguratuauto | Seguro Vehicular - {{$seguro1->nombre}}</title>

<meta name="description" content="Cotiza y contrata tu seguro vehicular en Aseguratuauto. Ahorra comparando opciones de seguro de autos con todas las aseguradoras del mercado en Perú.">
<meta name="keywords" content="seguro, seguro vehicular, vehiculo, soat, todo riesgo, rimac, pacificos seguros, HDI, Mapfre, positiva">
<meta name="author" content="Aseguratuauto.pe">

<meta itemprop="name" content="Aseguratuauto | Seguro Vehicular - {{$seguro1->nombre}}">
<meta itemprop="description" content="Cotiza y contrata tu seguro vehicular en Aseguratuauto. Ahorra comparando opciones de seguro de autos con todas las aseguradoras del mercado en Perú.">
<meta itemprop="image" content="{{ asset('asset/img/carro_1920.png') }}">

<meta property="og:title" content="Aseguratuauto | Seguro Vehicular - {{$seguro1->nombre}}" />
<meta property="og:url" content="{{ Request::url() }}" />
<meta property="og:image" content="{{ asset('asset/img/carro_1920.png') }}" />
<meta property="og:description" content="Cotiza y contrata tu seguro vehicular en Aseguratuauto. Ahorra comparando opciones de seguro de autos con todas las aseguradoras del mercado en Perú." />
<meta property="og:site_name" content="Aseguratuauto" />
@stop


@section('content')

<div id="fold">
    @include('principal.layouts.nav')
    <div class="home">
        <div class="container">
            <div class="row">
                <div class="home-izquierda col-xs-12 col-md-7">
                    <div class="logo col-xs-12 col-md-12">
                        <div class="contenedor-logo col-xs-12 col-md-5">    
                            <img src="{{asset('asset/img/logo-asegura.png')}}" alt="" class="hidden-xl hidden-lg hidden-md hidden-sm" style="max-width: 150px;">
                            <img src="{{asset('uploads/'.$seguro1->imagen_header)}}" style="margin-top: 15px;">
                        </div>
                        <div class="respaldo col-xs-12 col-md-7">
                            <p>Con el respaldo de</p>
                            <img src="{{asset('asset/img/logos/logo-hermes_blanco.png')}}">
                        </div>
                    </div>
                    <div class="home-izquierda-content col-xs-12 col-md-12">
                        <h1>COTIZA TU SEGURO VEHICULAR </h1>
                        <h5>ENCUENTRA LA MEJOR OPCIÓN EN SEGUNDOS</h5>
                        <h3>Te asesoramos GRATIS</h3>
                        <div class="poner-telefono col-md-12">
                            <div class="input-telefono col-md-7">
                                <input type="text" class="form-control" placeholder="Escribe tu teléfono" id="nro" style="margin-top: 5px;margin-bottom: 5px;">
                            </div>
                            <div class="boton-llamar col-md-3">
                                <button type="button" class="btn-llamar" id="formCotizarButton">Llámame</button>
                            </div>
                        </div>
                        <p class="hidden-xs hidden-sm">Un asesor se pondrá encontacto contigo</p>
                        <p class="hidden-md hidden-lg hidden-xl">Estamos asociados con las <br> <b>mejores aseguradoras</b></p>
                        <p class="hidden-xs hidden-sm">Estamos asociados con las mejores aseguradoras</p>
                        @include('principal.modulo.seguros', array('tipo' => 'derecha'))
                    </div>
                </div>
                <div class="home-derecha col-xs-12 col-md-5" id="homederecha">
                    @include('principal.modulo.form', array('tipo' => 'derecha'))
                </div>
            </div>
        </div>
    </div>
</div>
@include('principal.modulo.marcas-autos')
<section class="page-extensions-texto">
    <div class="container">
        <div class="row" style="margin: 0 !important;">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <img src="{{asset('uploads/' . $seguro1->imagen )}}" alt="" style="width: 100%;max-width: 400px;">
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <h3 class="titulo-naranja-centrado" style="    text-align: left !important;
    font-size: 30px;
    font-weight: 700;
    color: #3E2830;margin-bottom: 15px !important;">SEGURO DE AUTOS {{ strtoupper($seguro1->nombre) }}</h3>
                <p>{{ nl2br($seguro1->descripcion) }}</p>
            </div>
        </div>
    </div>
</section>
@stop


@section('css')
<style type="text/css">
.navbar-initial .logo-scroll {
    opacity: 1 !important;
}
.navbar-initial .logo-scroll img{
    filter: brightness(0) invert(1);
}
</style>
@stop