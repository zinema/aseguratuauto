<div class="funciona-wrapper" id="marca-definida" style="background: #fff">
	<div class="container funciona">
		<div class="row">
			<div class="col-sm-9 col-sm-push-3">
				<div class="row">
					<div class="col-sm-10 col-sm-push-1">
						<h2 class="text-center"><span>Cotizaciones anteriores</span></h2>
						<div class="row pasos">
							<div class="col-sm-4">
								<div class="paso text-center">
									<img src="{{asset('static/images/fun-1.png')}}" alt="">
								</div>
								<p class="text-center">En la parte superior de la web puedes colocar tu teléfono o llámanos al 6409749</p>
							</div>
							<div class="col-sm-4">
								<div class="paso text-center">
									<img src="{{asset('static/images/fun-2.png')}}" alt="">
								</div>
								<p class="text-center">Nuestros asesores te ayudarán a encontrar el seguro vehicular que más te conviene.</p>
							</div>
							<div class="col-sm-4">
								<div class="paso text-center">
									<img src="{{asset('static/images/fun-3.png')}}" alt="">
								</div>
								<p class="text-center">Ya que conoces el seguro vehicular que te conviene, nosotros te ayudamos a encontrar la mejor forma de pagarlo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>