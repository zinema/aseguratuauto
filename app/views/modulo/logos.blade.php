<div class="row logos">
    <div class="col-sm-12 text-center">
        <a href="{{route('principal.seguro','lapositiva')}}" target="_blank">
            <img src="{{asset('static-app/img/c1-on.png')}}" alt="" class="on">
            <img src="{{asset('static-app/img/c1.png')}}" alt="" class="off">
        </a>
        <a href="{{route('principal.seguro','rimac')}}" target="_blank">
            <img src="{{asset('static-app/img/c2-on.png')}}" alt="" class="on">
            <img src="{{asset('static-app/img/c2.png')}}" alt="" class="off">
        </a>
        <a href="{{route('principal.seguro','pacificoseguros')}}" target="_blank">
            <img src="{{asset('static-app/img/c3-on.png')}}" alt="" class="on">
            <img src="{{asset('static-app/img/c3.png')}}" alt="" class="off">
        </a>
        <a href="{{route('principal.seguro','mapfre')}}" target="_blank">
            <img src="{{asset('static-app/img/c4-on.png')}}" alt="" class="on">
            <img src="{{asset('static-app/img/c4.png')}}" alt="" class="off">
        </a>
        <a href="{{route('principal.seguro','hdiseguros')}}" target="_blank">
            <img src="{{asset('static-app/img/c5-on.png')}}" alt="" class="on">
            <img src="{{asset('static-app/img/c5.png')}}" alt="" class="off">
        </a>
    </div>
</div>




