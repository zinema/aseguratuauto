<div id="collapse">
  <div class="title">
    <h3 style="    color: #FFF;
    font-weight: bold;
    margin-bottom: 20px;">Preguntas Frecuentes</h3>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php

        $count = 0;
        $in = '';

        ?>
        @foreach($preguntas as $key => $pregunta)
          <?php
            if ($count == 0) {
              $in = 'in';
            }
          ?>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{$pregunta->id}}">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$pregunta->id}}" aria-expanded="true" aria-controls="collapse{{$pregunta->id}}">
                <h4 class="panel-title">
                  {{ $pregunta->titulo }}
                  <i class="material-icons">keyboard_arrow_down</i>
                </h4>
              </a>
            </div>
            <div id="collapse{{$pregunta->id}}" class="panel-collapse collapse {{$in}}" role="tabpanel" aria-labelledby="heading{{$pregunta->id}}">
              <div class="panel-body">{{ $pregunta->contenido }}</div>
            </div>
          </div>
          <?php
            $count = $count + 1;
            $in = '';
          ?>
        @endforeach
      </div>
    </div>
  </div>
</div>