<style type="text/css">
input#nro::-webkit-input-placeholder {
    font-size: 18px;
    line-height: 3;
}
</style>
<div class="asesoramos">
    <div class="ase-1">Te asesoramos <strong>gratis</strong></div>
    <div class="ase-2" style="margin-top: 5px;font-weight: 400;">Un asesor se pondrá en contacto contigo</div>
    <form action="" class="contacto-header formCotizar" id="formCotizar">
        <input type="hidden" name="dst" value="200">
        <div class="form-group numero">
            <input type="text" class="form-control telefono" id="nro" placeholder="Escribe tu teléfono" style="background-color: #ffffff !important;border: 0 !important;background-position: center bottom !important;-webkit-border-radius: 3px !important;-moz-border-radius: 3px !important;border-radius: 3px !important;color: #000 !important;padding-left: 9px;box-shadow: none !important;background: #ffffff !important;    font-size: 25px;">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-call-us boton btn-lg btn-block enviar enviar-telefono" style="background:url({{ asset('static-app/img/fondo-boton.gif') }}) 0 center repeat-x;font-size: 28px !important;padding: 10px 20px !important;" href="#dialog2" id="enviar">Llámame ahora</button>
        </div>
    </form>
</div>