<section class="section beneficios">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="text-center titulo-naranja-centrado">Beneficios para nuestros afiliados</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow: hidden;">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="max-width: 980px;margin: 0 auto;">
                    <div class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <?php
                                $count = 0;
                            ?>
                            @foreach($images as $image)
                                @if($image->status == 1)
                                <?php
                                    if ($count == 0) {
                                        $active = 'active';
                                    }
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="{{$count}}" class="{{$active}}"></li>
                                <?php
                                    $count = $count + 1;
                                    $active = '';
                                ?>
                                @endif
                            @endforeach
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php
                                $count = 0;
                            ?>
                            @foreach($images as $image)
                                @if($image->status == 1)
                                <?php
                                    if ($count == 0) {
                                        $active = 'active';
                                    }
                                ?>
                                <div class="item {{$active}}">
                                    <img src="{{asset('static-app/img/'.$image->image)}}" alt="Asegura tu auto" class="img-responsive">
                                </div>
                                <?php
                                    $count = $count + 1;
                                    $active = '';
                                ?>
                                @endif
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <i class="material-icons">keyboard_arrow_left</i>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <i class="material-icons">keyboard_arrow_right</i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                <br>
                <h3 style="color: #353535;font-weight: 500;text-align: right;">Enterate que otros descuentos tenemos para ti</h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                <br>
                <a href="{{ asset( $pdf[0]->title ) }}" target="_blank" class="btn btn-call-us btn-lg btn-cartilla">
                    <img src="{{asset('static-app/img/icon-descargar.png')}}">
                    <h5>Descargar</h5>
                    <h3> CARTILLA DE BENEFICIOS</h3>
                </a>
            </div>
        </div>
    </div>
</section>