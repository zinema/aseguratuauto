<?php $marcas = DB::table('marca')->select('id_marca','name_marca')->get(); ?>
<?php $anios = DB::table('anio')->select('id_anio','anio_antig')->orderBy('anio_antig','desc')->get(); ?>
<?php $usos = DB::table('uso')->select('id_uso','uso')->orderBy('uso','asc')->get(); ?>


<form class="sky-form" id="formu" action="">
    <h3 class="text-center">Cotiza al instante llenando el formulario</h3>
    <br>
    <div class="row" style="max-width: 320px;margin: 0 auto;">
        <div class="col-xs-6" style="padding: 0 !important;"><h5 class="datos-vehiculo activo"><span>1</span>Datos del Vehículo</h5></div>
        <div class="col-xs-6" style="padding: 0 !important;"><h5 class="datos-personales"><span>2</span>Datos Personales</h5></div>
    </div>
    <div class="paso-1 content-form activo">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Marca:</h4>
                <select placeholder="Seleccionar Marca" type="text" class="form-control" required="true" id="marca">
                    <option value="" selected="selected"></option>
                    @foreach($marcas as $marca)
                    <option value="{{$marca->id_marca}}">{{$marca->name_marca}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Modelo:</h4>
                <select placeholder="Seleccionar Modelo" type="text" class="form-control" required="true" id="modelo">
                    <option value="" selected="selected"></option>
                </select>
                <i></i>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Tipo:</h4>
                <select placeholder="Seleccionar Tipo" type="text" class="form-control" required="true" id="tipo">
                    <option value="" selected="selected"></option>
                </select>
                <i></i>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Timón:</h4>
                <select placeholder="Seleccionar Timón" type="text" class="form-control" required="true">
                    <option value="1" selected>Original</option>
                    <option value="2">Cambiado</option>
                </select>
                <i></i>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Año Fabricación:</h4>
                <select placeholder="Seleccionar Año" type="text" class="form-control" required="true" id="anio">
                    @foreach($anios as $anio)
                    <option value="{{$anio->id_anio}}">{{$anio->anio_antig}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Auto:</h4>
                <select placeholder="Seleccionar Año" type="text" class="form-control tipo-auto" required="true" id="">
                    <option value="1" class="" selected>Nuevo</option>
                    <option value="2" class="">Usado</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Uso:</h4>
                <select id="usoSelect" class="form-control" required="true" >
                    @foreach($usos as $uso)
                    <option value="{{$uso->id_uso}}">{{$uso->uso}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Valor Actual:</h4>
                <input type="text" required="true" class="form-control">
            </div>
            <div class="col-md-4 col-sm-8 col-xs-12">
                <h4 style="margin-bottom: 10px !important;">Ubicación:</h4>
                <input type="radio" name="ubicacion" class="" id="lima" checked="checked">
                <label for="lima" style="margin: 0 !important;font-size: 15px;font-weight: 400;color: #3C4858;" >Lima</label>
                <input type="radio" name="ubicacion" class="" id="provincia">
                <label for="provincia" style="margin: 0 !important;font-size: 15px;font-weight: 400;color: #3C4858;">Provincia</label>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                <button id="ir-paso-2" class="btn btn-sm btn-call-us btn-block" style="padding: 6px 10px 6px 8px !important;max-width: 190px;margin: 0 0 0 auto;text-transform: uppercase !important;">Paso 2</button>
            </div>
        </div>
    </div>
    <br>
    <div class="paso-2 content-form">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <h4>Nombre Completo:</h4>
                <input type="text" class="form-control" required="true" >
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Email:</h4>
                <input type="text" class="form-control" required="true" >
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6" style="padding-left: 5px !important;padding-right: 5px !important;">
                <h4>Celular:</h4>
                <input type="text" class="form-control" required="true" >
            </div>

            <div class="text-center col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-6 col-sm-push-2 col-xs-12">
                <button id="enviar-info" type="submit" class="btn btn-sm btn-call-us btn-block" style="padding: 14px 10px 14px 10px !important;">Encuentra tu seguro vehicular</button>
            </div>
        </div>
    </div>
</form>