<div class="row">
    <div class="col-md-7">
        <div class="brand text-left">
            <h1>COTIZA TU SEGURO VEHICULAR {{ strtoupper($marca1->nombre) }}</h1>
            <h3 class="title">ENCUENTRA EL MEJOR SEGURO VEHICULAR EN SEGUNDOS</h3>
            <img src="{{ asset('static-app/img/' . $marca1->imagen_marca) }}" alt="" style="max-width: 200px;width: 100%;float: left;margin-top: 15px;">
        </div>
    </div>
    <div class="col-md-5">
        @include('modulo.form')
        <br>
    </div>
</div>