<div class="row">
    <div class="col-md-7">
        <div class="brand">
            <h1>COTIZA TU SEGURO VEHICULAR {{ strtoupper($seguro1->nombre) }}</h1>
            <h3 class="title">ENCUENTRA EL MEJOR SEGURO VEHICULAR EN SEGUNDOS</h3>
            <img src="{{asset('static-app/img/' . $seguro1->imagen )}}" alt="" style="max-width: 350px;width: 100%;">
        </div>
    </div>
    <div class="col-md-5">
        @include('modulo.form')
        <br>
    </div>
</div>