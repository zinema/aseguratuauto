<div class="row">
    <div class="col-md-7">
        <div class="brand">
            <h1>COTIZA TU SEGURO VEHICULAR</h1>
            <h3 class="title">ENCUENTRA EL MEJOR SEGURO VEHICULAR EN SEGUNDOS</h3>
            <div class="c2c" style="max-width: 320px;margin: 0 auto;">
                @include('modulo.asesoramos')
            </div>
        </div>
    </div>
    <div class="col-md-5">
        @include('modulo.form')
        <br>
    </div>
</div>