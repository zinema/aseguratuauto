<div class="row como-funciona" style="margin: 0 !important;">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h3 class="text-center titulo-naranja-centrado">¿Cómo funciona?</h3>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <img src="{{asset('static/images/fun-1.png')}}" alt="Compara en SeguroSimple.com">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8">
                <div class="row">
                    <h5 class="subtitulo-naranja">COTIZA</h5>
                </div>
                <div class="row">
                    <p class="parrafo-azul-grande">Cotiza tu seguro vehicular con todas las aseguradoras del mercado en minutos.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <img src="{{asset('static/images/fun-2.png')}}" alt="Asegura en SeguroSimple.com">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8">
                <div class="row">
                    <h5 class="subtitulo-naranja">COMPARA</h5>
                </div>
                <div class="row">
                    <p class="parrafo-azul-grande">Nuestros asesores te ayudarán a encontrar el seguro vehicular que más te conviene.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <img src="{{asset('static/images/fun-3.png')}}" alt="Ahorra en SeguroSimple.com">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8">
                <div class="row">
                    <h5 class="subtitulo-naranja">CONTRATA</h5>
                </div>
                <div class="row">
                    <p class="parrafo-azul-grande">Ya que conoces el seguro vehicular que te conviene, nosotros te ayudamos a que lo contrates.</p>
                </div>
            </div>
        </div>
    </div>
</div>