@extends('dashboard')

@section('admin-content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                            <div class="header">
                                <h4 class="title">Agregar imagen</h4>
                            </div>
                            <div class="content">
                                <form class="multi-page-form" method="POST" action="{{route('add.image') }} " enctype="multipart/form-data">
                                @if($image)
                                <input type="hidden" name="image_id" value="{{$image->id}}">
                                @endif
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="imagen" class="control-label col-md-4">Imagen</label>
                                                <label class="solid col-md-6" for="imagen" style="background-color: #32c5d2;padding: 7px 20px 10px;color: #FFFFFF;font-size: initial;" id="fotocheck">Agregar imagen</label>
                                                <input type="file" id="imagen" style="display: none" name="image">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Estado</label>
                                                <select name="status" class="form-control">
                                                @if($image)
                                                    @if($image->status == 0)
                                                        <option value="0" selected="true">No activo</option>
                                                        <option value="1">Activo</option>
                                                    @else
                                                        <option value="0">No activo</option>
                                                        <option value="1" selected="true">Activo</option>
                                                    @endif
                                                @else
                                                     <option value="0">No activo</option>
                                                     <option value="1">Activo</option>
                                                @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Orden</label>
                                                <select name="orden" id="orden" class="form-control">
                                                    <option value="1">Primero</option>
                                                    <option value="2">Segundo</option>
                                                    <option value="3" >Tercero</option>
                                                    <option value="4">Cuarto</option>
                                                    <option value="5" >Quinto</option>
                                                    <option value="6" >Sexto</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-userpic" style="text-align: center;padding-top: 20px;border: 2px solid #e5e4e3;">
                                        @if($image)
                                            <label for="conductor_img">
                                                <img id="image-upload" src="{{ asset('static-app/img/'. $image->image) }}" class="img-responsive" alt=""> 
                                            </label>
                                        @else
                                            <label for="conductor_img">
                                                <img id="image-upload" src="#" class="img-responsive" alt=""> 
                                            </label>
                                        @endif
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar Imagen</button>
                                    </div>
                                    @if($image)
                                    <br><br><br>
                                    <div class="row" style="text-align:center;margin:auto">
                                        <div class="col-md-12">
                                            <a href="{{route('image','image_id='.$image->id).'&eliminar=1'}}" class="btn btn-danger btn-wd">Eliminar Marca</a>
                                        </div>
                                    </div>
                                    @endif
                                </form>
                            </div>
                        </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>

                    <li>
                        <a href="http://www.creative-tim.com">
                            Aseguratuauto
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                           Blog
                        </a>
                    </li>
                    <li>
                        <a href="http://www.creative-tim.com/license">
                            Licenses
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </footer>
@stop @section('js1')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-upload').attr('src', e.target.result);
                    $('#image-upload').width(250); 
                    $('#image-upload').height(250);
                    $('#image-upload').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imagen").change(function() {
            readURL(this);
        });

        $(function() {
            $("#orden").val({{$image->order}});
        });
    </script>
    @stop