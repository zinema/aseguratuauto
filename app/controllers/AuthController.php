<?php
 
class AuthController extends BaseController {
 
    public function showLogin()
    {
        if (Auth::check())
        {
            return Redirect::route('principal.home');
        }

        return View::make('admin.login');
    }
 
    public function postLogin(){
 		
        if (Auth::attempt(array('email' => Input::get('username'), 'password' => Input::get('password')))) 
        {
            return Redirect::intended('/');
        }

        // return Redirect::back()->with('error_message', 'Invalid data')->withInput();
        return Redirect::intended('/admin');
    }
 
    public function logOut()
    {
        Auth::logout();
        return Redirect::to('login')->with('error_message', 'Logged out correctly');
    }
 
}