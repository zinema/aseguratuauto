<?php

class AdminController extends BaseController {

	public function add_image()
	{
		$idImagen = Input::get('image_id');
		if ($idImagen != '') {
			$imagen = Imagen::find($idImagen);
		}else{
			$imagen = new Imagen;
		}

		$activo = Input::get('status');
		$orden = Input::get('orden');

		if(Input::hasFile('image')){
			$imageName = uniqid().".".Input::file('image')->getClientOriginalExtension();
			$upload = Input::file('image')->move(public_path().'/nuevo/uploads', $imageName);
			$img = Image::make($upload)->resize(966, 378);
			$imagen->image = $imageName;

		}

		$imagen->status = $activo;
		$imagen->order = $orden;
		$imagen->save();

		return Redirect::to('admin/image');
	}

	public function add_pdf()
	{

		$idPdf = Input::get('pdf_id');
		if ($idPdf != '') {
			$pdf = Pdf::find($idPdf);
		}else{
			$pdf = new Pdf;
		}

		if(Input::hasFile('pdf')){

			$fileName = uniqid().".".Input::file('pdf')->getClientOriginalExtension();
			$upload = Input::file('pdf')->move(public_path().'/nuevo/', $fileName);
			$name = Input::file('pdf')->getClientOriginalName();

			$pdf->title = $fileName;
			$pdf->original_title = $name;
			$pdf->save();
		}

		return Redirect::to('admin/pdf');
	}

	public function add_pregunta()
	{
		$idPregunta = Input::get('pregunta_id');

		if ($idPregunta != '') {
			$pregunta = Pregunta::find($idPregunta);
		}
		else{
			$pregunta = new Pregunta;
		}

		$titulo = Input::get('titulo');
		$contenido = Input::get('contenido');
		$orden = Input::get('orden');
		$pregunta->titulo = $titulo;
		$pregunta->contenido = $contenido;
		$pregunta->orden = $orden;
		$pregunta->save();

		return Redirect::to('admin/pregunta');
	}

	public function add_marca()
	{
		$nombre 		= Input::get('marca');
		$mostrar 		= Input::get('mostrar');
		$descripcion 	= Input::get('descripcion');
		$orden 			= Input::get('orden');
		
		$idMarca = Input::get('marca_id');
		if ($idMarca != '') {
			$marca = Marca::find($idMarca);
			$marca->seo    	  = Input::get('seo');
		}
		else{
			$marca = new Marca;
			$marca->seo    		 = strtolower($nombre);
		}

		if(Input::hasFile('imagen_marca')){

			$fileNameMarca = uniqid().".".Input::file('imagen_marca')->getClientOriginalExtension();
			$uploadMarca = Input::file('imagen_marca')->move(public_path().'/nuevo/uploads', $fileNameMarca);
			$nameMarca = Input::file('imagen_marca')->getClientOriginalName();
			$marca->imagen_marca = $fileNameMarca;
		}

		if(Input::hasFile('imagen_auto')){

			$fileNameAuto = uniqid().".".Input::file('imagen_auto')->getClientOriginalExtension();
			$uploadAuto = Input::file('imagen_auto')->move(public_path().'/nuevo/uploads', $fileNameAuto);
			$nameAuto = Input::file('imagen_auto')->getClientOriginalName();
			$marca->imagen_auto  = $fileNameAuto;
		}

		if(Input::hasFile('imagen_header')){

			$fileNameAuto = uniqid().".".Input::file('imagen_header')->getClientOriginalExtension();
			$uploadAuto = Input::file('imagen_header')->move(public_path().'/nuevo/uploads', $fileNameAuto);
			$nameAuto = Input::file('imagen_header')->getClientOriginalName();
			$marca->imagen_header  = $fileNameAuto;
		}

		$marca->orden 		 = $orden;
		$marca->nombre 		 = $nombre;
		$marca->descripcion  = $descripcion;
		$marca->status 	     = $mostrar;
		$marca->save();

		return Redirect::to('admin/marca');
	}

	public function add_seguro()
	{
		$nombre = Input::get('seguro');
		$mostrar = Input::get('mostrar');
		$descripcion = Input::get('descripcion');
		$imagen_header  = Input::get('imagen_header');

		$idSeguro = Input::get('seguro_id');
		if ($idSeguro != '') {
			$seguro = Seguro::find($idSeguro);
			$seguro->seo    	  = Input::get('seo');
		}
		else{
			$seguro = new Seguro;
			$seguro->seo    	  = strtolower(str_replace(' ', '', $nombre));
		}

		

		if(Input::hasFile('imagen_seguro')){

			$fileNameSeguro = uniqid().".".Input::file('imagen_seguro')->getClientOriginalExtension();
			$uploadSeguro = Input::file('imagen_seguro')->move(public_path().'/nuevo/uploads', $fileNameSeguro);
			$nameSeguro = Input::file('imagen_seguro')->getClientOriginalName();
			$seguro->imagen 	  = $fileNameSeguro;
		}

		if(Input::hasFile('imagen_header')){

			$fileNameAuto = uniqid().".".Input::file('imagen_header')->getClientOriginalExtension();
			$uploadAuto = Input::file('imagen_header')->move(public_path().'/nuevo/uploads', $fileNameAuto);
			$nameAuto = Input::file('imagen_header')->getClientOriginalName();
			$seguro->imagen_header  = $fileNameAuto;
		}

	
		$seguro->nombre 	  = $nombre;
		$seguro->descripcion  = $descripcion;
		$seguro->status 	  = $mostrar;
		$seguro->save();

		return Redirect::to('admin/seguro');
	}
}