<?php
class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('admin.dashboard');
	}

	public function images()
	{
		$idImage = Input::get('image_id');
		if ($idImage != '') {
			$image = Imagen::where('id',$idImage)->first();
			if ($image and Input::get('eliminar') == 1) {
				$objeto = Imagen::find($idImage);
				$objeto->delete();
			}
		}

		return View::make('admin.image');
	}

	public function new_image()
	{

		$image = '';
		$idImage = Input::get('image_id');

		if ($idImage != '') {
			$image = Imagen::where('id',$idImage)->first();
		}

		return View::make('admin.new_image', compact('image'));
	}

	public function pdf()
	{
		$idPdf = Input::get('pdf_id');
		if ($idPdf != '') {
			$pdf = Pdf::where('id',$idPdf)->first();
			if ($pdf and Input::get('eliminar') == 1) {
				$objeto = Pdf::find($idPdf);
				$objeto->delete();
			}
		}

		return View::make('admin.pdf');
	}

	public function new_pdf()
	{
		$pdf = '';
		$idPdf = Input::get('pdf_id');

		if ($idPdf != '') {
			$pdf = Pdf::where('id',$idPdf)->first();
		}

		return View::make('admin.new_pdf', compact('pdf'));
	}

	public function preguntas()
	{
		$idPregunta = Input::get('pregunta_id');
		if ($idPregunta != '') {
			$pregunta = Pregunta::where('id',$idPregunta)->first();
			if ($pregunta and Input::get('eliminar') == 1) {
				$objeto = Pregunta::find($idPregunta);
				$objeto->delete();
			}
		}

		return View::make('admin.pregunta');
	}

	public function new_pregunta()
	{
		$pregunta = '';
		$idPregunta = Input::get('pregunta_id');
		if ($idPregunta != '') {
			$pregunta = Pregunta::where('id',$idPregunta)->first();
		}
		return View::make('admin.new_pregunta',compact('pregunta'));
	}

	public function marcas()
	{
		$idMarca = Input::get('marca_id');
		if ($idMarca != '') {
			$marca = Marca::where('id',$idMarca)->first();
			if ($marca and Input::get('eliminar') == 1) {
				$objeto = Marca::find($idMarca);
				$objeto->delete();
			}
		}
		return View::make('admin.marca');
	}

	public function new_marca()
	{
		$marca = '';
		$idMarca = Input::get('marca_id');

		if ($idMarca != '') {
			$marca = Marca::where('id',$idMarca)->first();
		}

		return View::make('admin.new_marca',compact('marca'));
	}

	public function seguros()
	{
		$idSeguro = Input::get('seguro_id');
		if ($idSeguro != '') {
			$seguro = Seguro::where('id',$idSeguro)->first();
			if ($seguro and Input::get('eliminar') == 1) {
				$objeto = Seguro::find($idSeguro);
				$objeto->delete();
			}
		}

		return View::make('admin.seguro');
	}

	public function new_seguro()
	{
		$seguro = '';
		$idSeguro = Input::get('seguro_id');

		if ($idSeguro != '') {
			$seguro = Seguro::where('id',$idSeguro)->first();
		}

		return View::make('admin.new_seguro',compact('seguro'));
	}
}