<?php

class PrincipalController extends BaseController {

	public function home()
	{
		$marca = DB::table('marca')->select('id_marca','name_marca')->get();
		$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
		$anio = DB::table('anio')->select('id_anio','anio_antig')->orderBy('anio_antig','desc')->get();
		$images = DB::connection('mysql2')->table('images')->where('status', 1)->take(5)->orderBy('order', 'asc')->get();
		$pdf = DB::connection('mysql2')->table('files')->take(1)->orderBy('id', 'desc')->get();
		$preguntas = DB::connection('mysql2')->table('preguntas')->take(10)->orderBy('id', 'desc')->get();
		$marcas = DB::connection('mysql2')->table('marcas')->where('status', 1)->get();
		$seguros = DB::connection('mysql2')->table('seguros')->where('status', 1)->get();

		return View::make('principal.home', compact('marca','uso','anio', 'images', 'pdf', 'preguntas','marcas', 'seguros'));
	}

	public function seguro($seguroSeo)
	{
		$marca = DB::table('marca')->select('id_marca','name_marca')->get();
		$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
		$anio = DB::table('anio')->select('id_anio','anio_antig')->orderBy('anio_antig','desc')->get();
		$images = DB::connection('mysql2')->table('images')->take(3)->get();
		$pdf = DB::connection('mysql2')->table('files')->take(1)->orderBy('id', 'desc')->get();
		$preguntas = DB::connection('mysql2')->table('preguntas')->take(10)->orderBy('id', 'desc')->get();
		$marcas = DB::connection('mysql2')->table('marcas')->where('status', 1)->get();
		$seguros = DB::connection('mysql2')->table('seguros')->where('status', 1)->get();
		$seguro1 = DB::connection('mysql2')->table('seguros')->where('seo', $seguroSeo)->first();

		if($seguro1 != ''){
			return View::make('principal.seguro', compact('marca','uso','anio','seguroSeo', 'images', 'pdf', 'preguntas','marcas', 'seguros', 'seguro1'));
		}else{
			return View::make('principal.home', compact('marca','uso','anio', 'images', 'pdf', 'preguntas','marcas', 'seguros'));
		}

	}

	public function marca($marcaSeo)
	{
		$marca = DB::table('marca')->select('id_marca','name_marca')->get();
		$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
		$anio = DB::table('anio')->select('id_anio','anio_antig')->orderBy('anio_antig','desc')->get();
		$images = DB::connection('mysql2')->table('images')->take(3)->get();
		$pdf = DB::connection('mysql2')->table('files')->take(1)->orderBy('id', 'desc')->get();
		$preguntas = DB::connection('mysql2')->table('preguntas')->take(10)->orderBy('id', 'desc')->get();
		$marcas = DB::connection('mysql2')->table('marcas')->where('status', 1)->get();
		$marca1 = DB::connection('mysql2')->table('marcas')->where('seo', $marcaSeo)->first();
		$seguros = DB::connection('mysql2')->table('seguros')->where('status', 1)->get();

		if($marca1 != ''){
			return View::make('principal.marca', compact('marca','uso','anio', 'marcaSeo', 'images', 'pdf', 'preguntas','marcas','marca1', 'seguros'));
		}else{
			return View::make('principal.home', compact('marca','uso','anio', 'images', 'pdf', 'preguntas','marcas', 'seguros'));
		}


	}

	public function gracias()
	{
		$marca = DB::table('marca')->select('id_marca','name_marca')->get();
		$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
		$anio = DB::table('anio')->select('id_anio','anio_antig')->orderBy('anio_antig','desc')->get();
		$images = DB::connection('mysql2')->table('images')->take(3)->get();
		$pdf = DB::connection('mysql2')->table('files')->take(1)->orderBy('id', 'desc')->get();
		$preguntas = DB::connection('mysql2')->table('preguntas')->take(10)->orderBy('id', 'desc')->get();
		$marcas = DB::connection('mysql2')->table('marcas')->where('status', 1)->get();
		$seguros = DB::connection('mysql2')->table('seguros')->where('status', 1)->get();

		return View::make('principal.gracias', compact('marca','uso','anio', 'images', 'pdf', 'preguntas','marcas', 'seguros'));
	}

}
