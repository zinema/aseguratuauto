<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', array('as'=>'principal.home', 'uses' =>'PrincipalController@home'));
Route::get('/gracias', array('as'=>'principal.gracias', 'uses' =>'PrincipalController@gracias'));
Route::get('/seguro/{seguro}', array('as'=>'principal.seguro', 'uses' =>'PrincipalController@seguro'));
Route::get('/marca/{marca}', array('as'=>'principal.marca', 'uses' =>'PrincipalController@marca'));

Route::get('/resultado', function(){
  		
	return View::make('resultado.main', compact('marca','uso','anio'));
});

Route::get('/nuevo', function(){

	$marca = DB::table('marca')->select('id_marca','name_marca')->get();
	$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
	$anio = DB::table('anio')->select('id_anio','anio_antig')->get();
  		
	return View::make('old.principal.home', compact('marca','uso','anio'));
});

Route::get('emails/default', function(){

	return View::make('emails.default');

});

Route::get('prueba-curl', function(){

	$url = 'https://www.segurosimple.com/General/Busqueda/BuscarModelosPorMarcaAnio?Marca=53&Anio=2017';

	$ch = curl_init();
	$timeout = 5;

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

	$data = curl_exec($ch);

	curl_close($ch);

	echo $data;

});

// Route::get('/modelos/lista', function(){

// 	$marca = DB::table('marca')->select('id_marca','name_marca')->get();
// 	$uso = DB::table('uso')->select('id_uso','uso')->whereIn('id_uso',array(1,2,4,17,20))->get();
// 	$anio = DB::table('anio')->select('id_anio','anio_antig')->get();
  		
// 	return View::make('old.principal.home', compact('marca','uso','anio'));
// });

Route::post('/modelos/lista', array('as' => 'modelos.lista', function()
{
    $marca = Input::get('id_marca');

	$modelos = DB::table('auto_valor')
			  	->join('modelo','auto_valor.id_modelo','=','modelo.id_model')
			  	->where('auto_valor.id_marca','=',$marca)
			  	->select('modelo.id_model','modelo.name_model')
			  	->get();
		echo '<option value="?">Elige modelo</option>';
	foreach ($modelos as $key => $modelo) {
		echo '<option value="'.$modelo->id_model.'">'.$modelo->name_model.'</option>';
	}

}));

Route::post('/tipo/lista', array('as' => 'tipo.lista', function()
{
    $modelo = Input::get('id_modelo');

	$tipos = DB::table('auto_valor')
			  	->join('clase','auto_valor.id_tipo','=','clase.id_clase')
			  	->where('auto_valor.id_modelo','=',$modelo)
			  	->select('clase.id_clase','clase.clase')
			  	->get();
	echo '<option value="?">Elige tipo</option>';
	foreach ($tipos as $key => $tipo) {
		echo '<option value="'.$tipo->id_clase.'">'.$tipo->clase.'</option>';
	}

}));


Route::get('/enviar-correo-prueba', function(){

		// if ( Input::get('email') != 'sergiosaavedratorres@gmail.com' ) {
		// 	return 'hola';
		// }

		$emisorEmail 	= 'aseguratuauto@hermes.pe';
		$receptorEmail 	= 'aseguratuauto@hermes.pe';
		$emisorName 	= 'Asegura tu Auto - Cotización Web';
		$subjectFixed   = 'Asegura tu Auto - Cotización Web';
		$mensaje 		= 'Defecto';

		$marca   	=		Input::get('marca');
		$marca2    	= 		Input::get('marca2');
		$modelo   	=		Input::get('modelo');
		$modelo2   	=		Input::get('modelo2');
		$anio   	=		Input::get('anio');
		$anio2   	=		Input::get('anio2');
		$valor   	=		Input::get('valor');
		$uso   		=		Input::get('uso');
		$uso2   	=		Input::get('uso2');
		$name   	=		Input::get('nombre');
		$fono   	=		Input::get('fono');
		$email   	=		Input::get('email');

		$mensaje = ' ... marca: '.$marca.' ... marca2 : '.$marca2 .' ... modelo: '.$modelo.' ... modelo2: '.$modelo2.' ... anio: '.$anio.' ... anio2: '.$anio2.' ... valor: '.$valor.' ... uso: '.$uso.' ... uso2: '.$uso2.' ... name: '.$name.' ... fono: '.$fono.' ... email: '.$email;
		$receptorEmail = Input::get('email');
		
		$nombreCliente = $name;

	    Mail::send('emails.default', array('mensaje'=>$mensaje, 'nombreCliente'=>$nombreCliente), function($message) use ($emisorEmail,$emisorName, $receptorEmail, $subjectFixed)
	    {
	        $message->from($emisorEmail, $emisorName);
	        $message->to($receptorEmail);
	        $message->subject($subjectFixed);
	    });

	    return 'correo enviado';


});


Route::get('/enviar-pdf', function(){

		$emisorEmail 	= 'aseguratuauto@hermes.pe';
		$receptorEmail 	= 'aseguratuauto@hermes.pe';
		$emisorName 	= 'Asegura tu Auto - Cotización Web';
		$subjectFixed   = 'Asegura tu Auto - Cotización Web';
		$mensaje 		= 'Defecto';

		$receptorEmail = 'sergiosaavedratorres@gmail.com';
		
		$nombreCliente = $name;

		$file_pdf = asset('CARTILLA-2017.pdf');

	    Mail::send('emails.default', array('mensaje'=>$mensaje, 'nombreCliente'=>$nombreCliente), function($message) use ($emisorEmail,$emisorName, $receptorEmail, $subjectFixed,$file_pdf)
	    {
	        $message->from($emisorEmail, $emisorName);
	        $message->to($receptorEmail);
	        $message->subject($subjectFixed)
	        $message->attach(use ($file_pdf));;
	    });

	    return 'correo enviado';


});

// Mail::later(20,'emails.formFollowup', array('status'=>$status, 'JobNumber'=>$apmformdata->JobNumber), function($message) use ($file_to_save) {
//         $message->to('name@domain.com', 'My Name')
//             ->subject('APM Form Alert Notice')
//             ->attach(use ($file_to_save));
//     });