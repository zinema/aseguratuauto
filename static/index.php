<?php

function Conectarse(){
  if (!($link=mysql_connect('104.236.232.222','root','h3rm3$d4t4b4$3*'))){
     echo "Error conectando a la base de datos.";
     exit();
  }
  if (!mysql_select_db('cotizador',$link)){
     echo "Error seleccionando la base de datos.";
     exit();
  }
  return $link;
}

$link=Conectarse();

mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET COLLATION_CONNECTION = 'utf8_unicode_ci'");

$marca = mysql_query("SELECT id_marca, name_marca FROM marca",$link);
$uso = mysql_query("SELECT id_uso, uso FROM uso WHERE id_uso=1 or id_uso=2 or id_uso=5 or id_uso=17 or id_uso=20 ",$link);
$anio = mysql_query("SELECT id_anio, anio_antig FROM anio",$link);  		

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>AseguraTuAuto.pe - Seguro Vehicular</title>
	<link href="bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="bootstrapValidator.min.css">
	<link href="style.css" rel="stylesheet">	
	<meta name="google-site-verification" content="XYVGSKnYcRPP36RI2tTTFLbMCfV0K4JGhKigl1V9bAQ" />
		</head>
		<body class="bg-inicio" id="seguros">
			<div class="inicio-wrapper" id="contactanos">
				<div id="boxes">
					<div id="dialog" class="window">
						<div class="call">
							<img src="images/call.png" alt="">
						</div>
						<div class="call2">
							<img src="images/logo-popa.png" alt=""><br><br>
							<p>En un momento te enviaremos la cotización a tu mail o nos pondremos en contacto contigo</p>
							<p class="call3">¡Gracias!</p>
						</div>					
					</div>
					<div id="mask"></div>
				</div>

				<div id="boxes2">
					<div id="dialog2" class="window2">
						<div class="call4">
							<img src="images/call.png" alt="">
						</div>
						<div class="call5">
							<img src="images/logo-popa.png" alt=""><br><br>
							<p>En un momento te estaremos llamando, muchas gracias!</p>
							<p class="call6">¡Gracias!</p>
						</div>
					</div>
					<div id="mask2"></div>
				</div>	

				<div class="container inicio">
					<div class="row">
						<div class="col-sm-9 col-sm-push-3">
							<div class="row hidden-xs">
								<div class="col-sm-12 logos">
									<a href="https://www.lapositiva.com.pe" target="_blank">
										<img src="images/c1-on.png" alt="" class="on">
										<img src="images/c1.png" alt="" class="off">
									</a>
									<a href="http://www.rimac.com.pe" target="_blank">
										<img src="images/c2-on.png" alt="" class="on">
										<img src="images/c2.png" alt="" class="off">
									</a>
									<a href="http://www.pacificoseguros.com" target="_blank">
										<img src="images/c3-on.png" alt="" class="on">
										<img src="images/c3.png" alt="" class="off">
									</a>
									<a href="http://www.mapfre.com.pe/" target="_blank">
										<img src="images/c4-on.png" alt="" class="on">
										<img src="images/c4.png" alt="" class="off">
									</a>
									<div class="telf pull-right">
										<div class="txt">Llámanos al</div>
										<div class="num">(01) 640-0778</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 intro text-center">
									<strong>Cotiza</strong> de la manera más fácil y <strong>ahorra</strong> en tu <br> <strong>seguro vehicular</strong>
								</div>
							</div>
							<div class="row hidden-lg hidden-md hidden-sm">
								<div class="col-sm-12 logos">
									<a href="https://www.lapositiva.com.pe" target="_blank">
										<img src="images/c1-on.png" alt="" class="on">
										<img src="images/c1.png" alt="" class="off">
									</a>
									<a href="http://www.rimac.com.pe" target="_blank">
										<img src="images/c2-on.png" alt="" class="on">
										<img src="images/c2.png" alt="" class="off">
									</a>
									<a href="http://www.pacificoseguros.com" target="_blank">
										<img src="images/c3-on.png" alt="" class="on">
										<img src="images/c3.png" alt="" class="off">
									</a>
									<a href="http://www.mapfre.com.pe/" target="_blank">
										<img src="images/c4-on.png" alt="" class="on">
										<img src="images/c4.png" alt="" class="off">
									</a>
								</div>
							</div>
							<div class="row hidden-xs">
								<div class="col-lg-4 col-lg-push-4 col-md-6 col-md-push-3 col-sm-8 col-sm-push-2 asesoramos">
									<div class="ase-1">Te asesoramos <strong>gratis</strong></div>
									<div class="ase-2">Un asesor se pondrá en contacto contigo</div>

										<form action="" class="contacto-header" id="formCotizar">
											<input type="hidden" name="dst" value="200">
											<div class="form-group">
												<input type="text" class="form-control telefono" id="nro" placeholder="Escribe tu teléfono">
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary btn-block enviar">Llámame ahora</button>
											</div>
										</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="flotante-wrapper" id="cotizador">
				<div class="container flotante">
					<div class="menu-movil-wrapper hidden-lg hidden-md hidden-sm">
						<div class="menu-movil">
							<div class="navbar navbar-default">
								<div class="navbar-header">
									<div class="logo-movil"></div>
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a href="#contacto" class="link-c">Contacto</a>
								</div>
								<div class="navbar-collapse collapse navbar-responsive-collapse">
									<ul class="nav navbar-nav">
										<li><a href="#seguros">Seguros</a></li>
										<li><a href="#cotizador">Cotizador</a></li>
										<li><a href="#contacto">Contáctanos</a></li>
										<li><a href="#como-funciona">¿Cómo funciona?</a></li>
										<li><a href="#por-que-un-broker">¿Por qué un broker?</a></li>
										<li><a href="#preguntas-frecuentes">Preguntas Frecuentes</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row sup-m">
						<div class="col-sm-3">
							<div class="col-izq">
								<div class="logo-asegura hidden-xs"></div>
								<div class="logo-asegura-fake hidden-xs"></div>
								<div class="menu-prin hidden-xs">
									<span class="menu-text">Menú</span>
									<div class="menu-boton">
										<span class="bb"></span>
										<span class="bb"></span>
										<span class="bb"></span>
										<div class="menu-desple">
											<ul>
												<li><a href="#contactanos">Contáctanos</a></li>
												<li><a href="#como-funciona">¿Cómo funciona?</a></li>
												<li><a href="#por-que-un-broker">¿Por qué un broker?</a></li>
												<li><a href="#preguntas-frecuentes">Preguntas Frecuentes</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="intro-formu hidden-xs">
									Coloca los datos de <br> tu auto aquí:
								</div>
								<!--<div class="formu">-->
								<form action="formularioAsegura.php" id="formu" class="formu">								
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Marca</label>
												<!--<input class="form-control" value="" type="text">-->
												<select name="marca" class="form-control" id="marca">
													<option value=""></option>
													<?php 
													while ( $resmarca = mysql_fetch_array($marca)) {
													?>
													<option value="<?= $resmarca['id_marca']; ?>"><?= $resmarca['name_marca']; ?></option>
													<?php 
														}
													?>											
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-12 col-xs-6 coco1">
											<div class="form-group">
												<label class="control-label">Modelo</label>
												<select name="modelo" class="form-control" id="modelo"></select>
											</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-6 coco2">
											<div class="form-group">
												<label class="control-label">Año Fabricación</label>
												
												<select name="anio" class="form-control" id="anio">
													<option value=""></option>
													<?php 
													while ( $resanio = mysql_fetch_array($anio)) {
													?>
													<option value="<?= $resanio['id_anio']; ?>"><?= $resanio['anio_antig']; ?></option>
													<?php 
														}
													?>											
												</select>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-12 col-xs-6 coco1">
											<div class="form-group">
												<label class="control-label">Valor Mercado - $</label>
												<input class="form-control" value="" type="text" name="valor">
											</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-6 coco2">
											<div class="form-group">
												<label class="control-label">Uso</label>
												<!--<input class="form-control" value="" type="text">-->
												<select name="uso" class="form-control" id="uso">
													<option value=""></option>
													<?php 
													while ( $resuso = mysql_fetch_array($uso)) {
													?>
													<option value="<?= $resuso['id_uso']; ?>"><?= $resuso['uso']; ?></option>
													<?php 
														}
													?>											
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Nombre y Apellido</label>
												<input class="form-control" value="" type="text" name="nombre">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Teléfono</label>
												<input class="form-control" value="" type="text" name="fono">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">E-mail</label>
												<input class="form-control" value="" type="email" name="email" required="">
											</div>
										</div>
									</div>
									<div class="row">
											<div class="col-sm-12 text-center">
											<div class="form-group">
												<div class="checkbox">
													<input checked="checked" type="checkbox" name="otro" /> Acepto los términos y condiciones
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 text-center">
											<button type="submit" class="btn btn-default cotizar" href="#dialog">Cotizar</button>
										</div>
									</div>
								</form>
								<!--</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="aseso-m-wrapper hidden-lg hidden-md hidden-sm" id="contacto">
				<div class="container aseso-m">
					<div class="row">
						<div class="col-lg-4 col-lg-push-4 col-md-6 col-md-push-3 col-sm-8 col-sm-push-2 asesoramos">
							<div class="ase-1">Te asesoramos <strong>gratis</strong></div>
							<div class="ase-2">Un asesor se pondrá en contacto</div>
										<form class="contacto-header hidden-sm hidden-xs" id="formCotizar">
											<input type="hidden" name="dst" value="200">
											<div class="form-group">
												<input type="text" class="form-control telefono" id="nro" placeholder="Escribe tu teléfono">
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary btn-block enviar" href="#dialog2" id="enviar">Llámame ahora</button>
											</div>
										</form>
										<form class="contacto-header hidden-md hidden-lg" id="formCotizar2">
											<input type="hidden" name="dst" value="200">
											<div class="form-group">
												<input type="text" class="form-control telefono" id="nro2" placeholder="Escribe tu teléfono">
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary btn-block enviar" href="#dialog2" id="enviar">Llámame ahora</button>
											</div>
										</form>
							<div class="txt-1">contáctanos al:</div>
							<div class="txt-2">(01) 640-0778</div>
						</div>
					</div>
				</div>
			</div>
			<div class="funciona-wrapper" id="como-funciona">
				<div class="container funciona">
					<div class="row">
						<div class="col-sm-9 col-sm-push-3">
							<div class="row">
								<div class="col-sm-10 col-sm-push-1">
									<h2 class="text-center"><span>¿Cómo Funciona?</span></h2>
									<div class="row pasos">
										<div class="col-sm-4">
											<div class="paso text-center">
												<img src="images/fun-1.png" alt="">
											</div>
											<p class="text-center">En la parte superior de la web puedes colocar tu teléfono o llámanos al 6409749</p>
										</div>
										<div class="col-sm-4">
											<div class="paso text-center">
												<img src="images/fun-2.png" alt="">
											</div>
											<p class="text-center">Nuestros asesores te ayudarán a encontrar el seguro vehicular que más te conviene.</p>
										</div>
										<div class="col-sm-4">
											<div class="paso text-center">
												<img src="images/fun-3.png" alt="">
											</div>
											<p class="text-center">Ya que conoces el seguro vehicular que te conviene, nosotros te ayudamos a encontrar la mejor forma de pagarlo.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="broker-wrapper" id="por-que-un-broker">
				<div class="container broker">
					<div class="row">
						<div class="col-sm-9 col-sm-push-3">
							<div class="row">
								<div class="col-sm-10 col-sm-push-1">
									<h2><span>¿Por qué un <strong>broker</strong>?</span></h2>
									<div class="row bloque-verde">
										<div class="col-lg-6 col-lg-push-6 col-md-5 col-md-push-7 col-sm-8 col-sm-push-2 bloque-verde-content">
											<p>El Broker de Seguros es fundamental, ya que funciona <strong>como asesor</strong> y aliado ante cualquier eventualidad (accidente, siniestro, etc) que puedas tener para la que se requiera el seguro vehicular.</p>
											<p class="txt-espe">Hermes Corredores de Seguros cuenta con más de <strong>25 años de experiencia</strong> brindando un servicio de excelencia las 24 horas los <strong>365 dìas del año.</strong></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="barra-gris hidden-xs"></div>
			</div>
			<div class="faq-wrapper" id="preguntas-frecuentes">
				<div class="container faq">
					<div class="row">
						<div class="col-sm-9 col-sm-push-3">
							<div class="row hidden-xs">
								<div class="col-sm-12 logos">
									<a href="https://www.lapositiva.com.pe" target="_blank">
										<img src="images/c1-on.png" alt="" class="on">
										<img src="images/c1.png" alt="" class="off">
									</a>
									<a href="http://www.rimac.com.pe" target="_blank">
										<img src="images/c2-on.png" alt="" class="on">
										<img src="images/c2.png" alt="" class="off">
									</a>
									<a href="http://www.pacificoseguros.com" target="_blank">
										<img src="images/c3-on.png" alt="" class="on">
										<img src="images/c3.png" alt="" class="off">
									</a>
									<a href="http://www.mapfre.com.pe/" target="_blank">
										<img src="images/c4-on.png" alt="" class="on">
										<img src="images/c4.png" alt="" class="off">
									</a>
									<div class="telf pull-right">
										<div class="txt">Llámanos al</div>
										<div class="num">(01) 640-0778</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-push-1 col-sm-10 col-sm-push-1">
									<h2>Preguntas Frecuentes</h2>
									<div class="faq-lista">
										<div class="faq-item activo">
											<div class="faq-q">
												¿Qué es un seguro vehicular?
											</div>
											<div class="faq-a">
												El seguro vehicular brinda cobertura y beneficios que cubre reparaciones en caso de accidente de tránsito e indemniza en caso de robo. Asimismo, ofrece beneficios como chofer de reemplazo, vehículo de reemplazo, servicio de auxilio mecánico, traslado del vehículo por averías al taller, entre otros, dependiendo de la póliza.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Por qué tener un seguro?
											</div>
											<div class="faq-a">
												Nunca podemos saber qué eventualidad puede suceder ni en qué momento, por lo cual contar con un seguro es una protección constante para ti y terceros.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Qué cubren los seguros vehiculares?
											</div>
											<div class="faq-a">
												Generalmente se ofrecen las siguientes coberturas para seguros vehiculares: Daño Propio. Mediante esta cobertura se indemniza por los daños que pueda sufrir el vehículo asegurado producto de accidentes como choque, robo, incencio, daños materiales, entre otros. Responsabilidad Civil frente a terceros. A través de esta cobertura se indemniza los daños ocasionados por el vehículo asegurado a propiedades de terceros; las lesiones que sufrieran los ocupantes de los vehículos afectados por el vehículo asegurado, las lesiones que sufrieran los peatones a consecuencia de un atropello del vehículo asegurado. Accidentes personales. La cobertura de accidentes personales indemniza por las lesiones que sufrieran los ocupantes del vehículo asegurado, producto de accidentes de tránsito.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Qué es una prima?
											</div>
											<div class="faq-a">
												Es el monto que el asegurado o contratante paga a la compañía de seguros a cambio de la cobertura que este le otorga.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Qué es el deducible?
											</div>
											<div class="faq-a">
												Es el monto mínimo o porcentaje de los gastos que le corresponde pagar al asegurado por el derecho de atención de un siniestro. La compañía pagará la diferencia entre el gasto total y el monto por el deducible.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Qué es un siniestro?
											</div>
											<div class="faq-a">
												Daño o pérdida, total o parcial que le ocurre al vehículo asegurado y a sus beneficiarios por la pérdida acaecida. En el caso de seguro vehicular, un siniestro puede ser un choque, robo, aluvión, rompedura de lunas, etc.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Qué es la suma asegurada?
											</div>
											<div class="faq-a">
												Monto máximo de cobertura económica que asumirá la compañía de seguros en caso de reparación, reposición o indemnización a favor del asegurado, producto de algún siniestro cubierto por la póliza.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿En qué casos se determina la pérdida total de un auto asegurado?
											</div>
											<div class="faq-a">
												Se puede dar por robo o choque. En el caso de choque, es la pérdida o daño que sufra el vehículo cuyo valor de reparación (incluyendo el IGV), alcance o supere el 70% del valor comercial o la suma asegurada.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Por qué Hermes Corredores de Seguros?
											</div>
											<div class="faq-a">
												Hermes tiene más de 25 años demostrando excelencia en el trato personalizado las 24 horas con sus clientes, sin un costo adicional, sino todo lo contrario, ya que al contar un seguro por este medio ahorras en promedio 15%.
											</div>
										</div>
										<div class="faq-item">
											<div class="faq-q">
												¿Cuál es la mejor aseguradora?
											</div>
											<div class="faq-a">
												Todas las aseguradoras con las que trabajamos son de altísima calidad, sin embargo dependiendo del tipo de seguro que desees, nosotros podemos recomendarte cuál es la mejor para ti.
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-lg-push-2 col-md-5 col-md-push-1 hidden-sm hidden-xs">
									<div class="asesoramos">
										<div class="ase-1">Te asesoramos <strong>gratis</strong></div>
										<div class="ase-2">Un asesor se pondrá en contacto contigo</div>
										<form action="" class="contacto-header" id="formCotizar">
											<input type="hidden" name="dst" value="200">
											<div class="form-group">
												<input type="text" class="form-control telefono" id="nro" placeholder="Escribe tu teléfono">
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary btn-block enviar">Llámame ahora</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<script src="jquery.js"></script>
			<script src="bootstrap.min.js"></script>
			<script src="bootstrapValidator.min.js"></script>
			<script src="main.js"></script>
			<script type="text/javascript">

				jQuery(document).ready(function($) {
				    
				    $('#mask').click(function mask() {
						$(this).hide()
						$('.window').hide()
				    })

				    $('#mask2').click(function mask2() {
						$(this).hide()
						$('.window2').hide()
				    })

				    /*$('#formu').on('submit', function popa(e) {
				    	e.preventDefault()
				    	e.stopPropagation()

				    	var form = $('#formu').serialize()

				    	$.ajax({
							url :'http://dilootu.com:60/aseguratuauto/formularioAsegura.php',
							type:'POST',
							data: form,
							complete:function(xhr,status){
							    console.log(status)
							}
					    })

				    	var id = $('#formu').find('.cotizar').attr('href')

							var maskHeight = $(document).height();
							var maskWidth = $(window).width();

							$('#mask').css({'width':maskWidth,'height':maskHeight});

							$('#mask').fadeIn(1000);	
							$('#mask').fadeTo("slow",0.8);	

							var winH = $(window).height();
							var winW = $(window).width();

							$(id).css('top',  winH/2-$(id).height()/2);
							$(id).css('left', winW/2-$(id).width()/2);

							$(id).fadeIn(2000);
				    })*/
					$('#formu').bootstrapValidator({
						message: 'Este valor no es valido',
						submitHandler: function(validator, form, submitButton) {

							var id = form.find('.cotizar').attr('href')

							var maskHeight = $(document).height();
							var maskWidth = $(window).width();

							$('#mask').css({'width':maskWidth,'height':maskHeight});

							$('#mask').fadeIn(1000);	
							$('#mask').fadeTo("slow",0.8);	

							var winH = $(window).height();
							var winW = $(window).width();

							$(id).css('top',  winH/2-$(id).height()/2);
							$(id).css('left', winW/2-$(id).width()/2);

							$(id).fadeIn(2000);
							var marca = $("#marca option:selected").html()
							var modelo = $("#modelo option:selected").html()
							var uso = $("#uso option:selected").html()
							var anio = $("#anio option:selected").html()
							$.post(form.attr('action'), form.serialize()+"&marca2="+marca+"&modelo2="+modelo+"&uso2="+uso+"&anio2="+anio, function(result) {

							}, 'json');
						},
						fields: {
							marca: {
								validators: {
									notEmpty: {
										message: 'Seleccione una marca'
									},
									stringLength: {
										min: 1,
										max: 100,
									}
								}
							},
							modelo: {
								validators: {
									notEmpty: {
										message: 'Seleccione un modelo'
									}
								}
							},
							uso: {
								validators: {
									notEmpty: {
										message: 'Seleccione un uso'
									}
								}
							},
							anio: {
								validators: {
									notEmpty: {
										message: 'Seleccione un Año'
									}
								}
							},
							valor: {
								validators: {
									notEmpty: {
										message: 'Escriba el valor'
									},
									stringLength: {
										min: 1,
										max: 100
									},
									digits: {
                       					message: 'Solo números'
                    				}
								}
							},
							nombre: {
								validators: {
									notEmpty: {
										message: 'Escriba su nombre'
									},
									stringLength: {
										min: 10,
										max: 100,
										message: 'Minimo 10 caracteres'
									}
								}
							},
							fono: {
								validators: {
									notEmpty: {
										message: 'Escriba su telefono'
									},
									stringLength: {
										min: 7,
										max: 9
									},
									digits: {
                       					message: 'Solo números'
                    				}
								}
							},
							email: {
								validators: {
									notEmpty: {
										message: 'Escriba su email'
									},
									emailAddress: {
                        				message: 'No es un email valido'
                    				}
								}
							},
							otro: {
								validators: {
									notEmpty: {
										message: 'Aceptar términos'
									}
								}
							},
						}
					})

					$('.faq-q').click(function(event) {
						$('.faq-item').removeClass('activo');
						$(this).parent().addClass('activo');
						$('.faq-a').slideUp(350);
						$(this).siblings('.faq-a').slideDown(350);
					});
					$('.menu-boton').click(function(event) {
						if($(this).parent().hasClass('on')){
							$(this).parent().removeClass('on');
						}else{
							$(this).parent().addClass('on');
						}
					});
					$('.menu-prin').mouseleave(function(event) {
						$(this).removeClass('on');
					});
					$('a').click(function(){
						$('html, body').animate({
							scrollTop: $( $.attr(this, 'href') ).offset().top
						}, 500);
						return false;
					});

					$("#marca").on('change', function traerModelo  () {
				    	$("#marca option:selected").each(function () {
				            id_marca = $(this).val();
				            $.post("modelos.php", { id_marca: id_marca }, function(data){
				                $("#modelo").html(data);
				            })          
				        })
				    })

					
				})

				$(function () {
					$('.navbar-collapse ul li a:not(.dropdown-toggle)').bind('click touchstart', function () {
						$('.navbar-toggle:visible').click();
					});
				});

			</script>
			<script> window.r1 = 0;r2=2;</script>
<!--			<script data-company="ddd69cdd-11e3-4b31-9a95-648124541ef3" id="__diloo__w" src="//widget.dilooapp.com/widget/ddd69cdd-11e3-4b31-9a95-648124541ef3?type=web"></script> -->
			<!--Start of Zendesk Chat Script-->
     <script type="text/javascript">
window.$zopim||(function(d,s){
      var z=$zopim=function(c){
          z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.push(o)};z=[];z.set=[];$.async=!0;$.setAttribute('charset','utf-8');$.src='https://v2.zopim.com/?4dk8Xz2bygCDU6boJZamYYm5pladqMHY';z.t=+new Date;$.type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
		</body>
		</html>
